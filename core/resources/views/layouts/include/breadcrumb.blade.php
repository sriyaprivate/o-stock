<div class="row wrapper white-bg page-heading page-header-main">
    <div class="col-lg-10">
        <h2>{{(isset($breadcrumbs))?$breadcrumbs['title']:''}}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            @if(isset($breadcrumbs))
            <li>
                <a>{{$breadcrumbs['prefix']}}</a>
            </li>
            @endif
            @if((isset($breadcrumbs)))
            <li class="active">
                <strong>{{$breadcrumbs['title']}}</strong>
            </li>
            @endif
        </ol>
    </div>
    <div class="col-lg-2 text-right">
        <!--<button class="btn btn-primary" type="button">Go Back</button>-->
    </div>
</div>