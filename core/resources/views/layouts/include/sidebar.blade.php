<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element text-center">
                    <span>
                        <img alt="image" class="img-circle" src="{{asset('assets/inspinia/img/avatar.png')}}"  width="20%" />
                    </span>
                    <a href="#">
                        <span class="clear" style="display:inline-block;vertical-align:middle;"> 
                            <span class="block m-t-xs">
                                <strong class="font-bold">
                                    @if($user)
                                        {{$user->name}}
                                    @else
                                        No One
                                    @endif
                                </strong>
                            </span>
                            <span class="text-muted text-xs block">Member</span>
                            <span class="text-muted text-xs block">Last: {{($user && $user->last_login)?$user->last_login:'-'}}</span>
                        </span>
                    </a>
                </div>
                <div class="logo-element">
                    AD+
                </div>
            </li>
            @include('layouts.include.sidemenu')
        </ul>

    </div>
</nav>