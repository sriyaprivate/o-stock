<div class="footer">
    <div class="pull-right">
        {{config('app.app_name')}} <strong>{{config('app.app_version')}}</strong>
    </div>
    <div>
        <strong>Copyright</strong> {{config('app.company_name')}} &copy; 2016-{{date('Y')}}
    </div>
</div>