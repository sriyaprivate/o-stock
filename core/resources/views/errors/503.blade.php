<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>CEAT | Admin Panel</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">
  <link rel="shortcut icon" href="/favicon.ico">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- BOOTSTRAP -->  
  <link rel="stylesheet" href="{{asset('assets/dist/bootstrap/css/bootstrap.min.css')}}">  
  <!-- STYLE -->
  <link rel="stylesheet" href="{{asset('assets/core/css/style.css')}}">


</head>

      <div class="row text-center" >
        <div class="boo-wrapper">
          <div class="boo">
            <div class="face"></div>
          </div>
          <div class="shadow"></div>

          <h1>503, Whoops!</h1>
          <p>
            Be right back
          </p>
        </div>        
      </div>

      <!-- modernizr -->
      <script src="{{asset('assets/dist/core/js/modernizr.js')}}"></script>
      <!-- jquery -->      
      <script src="{{asset('assets/dist/jquery/js/jquery-1.12.3.min.js')}}"></script>
      <!-- bootstrap -->      
      <script src="{{asset('assets/dist/bootstrap/js/bootstrap.min.js')}}"></script>
      

   
  </body>
</html>
