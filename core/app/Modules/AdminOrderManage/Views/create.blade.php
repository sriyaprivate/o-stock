@extends('layouts.back_master') @section('title','Purchase Order')
@section('css')
<style type="text/css">
    .btn-md-1{
        margin-bottom: 10px;
    }
    .pagination {
        display: inline-block;
        padding-left: 0;
        margin: 0 0; 
        border-radius: 4px;
    }
    .modal .overlay, .overlay-wrapper .overlay {
        z-index: 50;
        background: rgba(255,255,255,0.7);
        border-radius: 3px;
    }
    .modal .overlay, .overlay-wrapper>.overlay, .box>.loading-img, .overlay-wrapper>.loading-img {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    .modal  .overlay>.fa, .overlay-wrapper .overlay>.fa {
        position: absolute;
        top: 50%;
        left: 50%;
        margin-left: -15px;
        margin-top: -15px;
        color: #000;
        font-size: 30px;
    }
    .add-item{
      color: #6aa8e8;
    }
    .rem-item{
      color: #e45656;
    }

    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
      display: none !important;
    }

    .item-selected{
      background: #b0d3ff;
    }

    .error{
        color: red;
    }
</style>

@stop
@section('content')
<div  ng-app="app" ng-controller="AdminOrderController" >
    <form action="{{url('admin/order/create')}}" method="post" name="form" id="form">
    {!!Form::token()!!}
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-4">
                <h2>Order Management</h2>
                <ol class="breadcrumb">
                    <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
                    <li><a href="{{url('admin/grn/list')}}">Order Management</a></li>
                    <li class="active">Create Order</li>
                </ol>
            </div>
            <div class="col-lg-8">
                <div class="title-action @if($errors->has('customer_id')) has-error @endif">
                    <select class="form-control chosen" name="customer_id" id="customer_id" style="width:50%">
                        @foreach($customers as $customer)
                            <option value="{{$customer['id']}}">{{$customer['name']}}</option>
                        @endforeach
                    </select>
                    <button type="button" class="btn btn-md btn-primary" style="background-color: brown;border-color: brown" data-toggle="modal" href='#modal-items' ng-click="getItems(1,'')">Add Item</button>
                    @if($errors->has('customer_id'))
                        <span class="help-block">{{$errors->first('customer_id')}}</span>
                    @endif
                    <a class="btn btn-primary" href="{{ url('admin/order/list') }}"><i class="fa fa-th" aria-hidden="true"></i> Order</a>
                </div>
            </div>
        </div>
        <input type="hidden" name="warehouse" value="@{{warehouse}}">
        <div class="wrapper wrapper-content animated fadeInRight" ng-cloak >
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Customer to Warehouse - Purchase Order</h5>
                    <div class="ibox-tools">
                        
                    </div>
                </div>
                
                <div class="ibox-content" style="display: block;">
                    
                    <div class="row btn-md-1">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 @if($errors->has('warehouse_id')) has-error @endif">
                            <select class="form-control chosen" name="warehouse_id" id="warehouse_id" style="width:50%" ng-model="warehouse" ng-options="val.id as val.name for (ware , val) in warehouses" ng-init="warehouse=warehouses[0].id"></select>
                            @if($errors->has('warehouse_id'))
                                <span class="help-block">{{$errors->first('warehouse_id')}}</span>
                            @endif
                        </div>
                        
                        <div class="col-md-6">
                            <h4 class="text-right" style="margin-top:3px;margin-bottom: 0;">
                                <p class="text-right" style="margin-bottom: 0">
                                    ( Date : {{date('Y-m-d')}} )
                                </p>                                
                            </h4>          
                        </div>
                    </div>

                    <div class="row">
                        <table class="table table-bordered items-table" align="center" style="width: 98% !important">
                            <thead>
                                <tr>
                                    <th class="text-center" width="5%">#</th>
                                    <th class="text-center">Product / Code</th>
                                    <th class="text-right">Price (Rs)</th>
                                    <th class="text-right">Qty</th>
                                    <th class="text-right">Amount (Rs)</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="orderDetail in orderDetails" class="item-rows">
                                    <td class="text-center">
                                        <input type="hidden" name="product_id[]" value="@{{orderDetail.id}}">
                                        @{{$index+1}}
                                    </td>
                                    <td class="text-center">
                                        <strong>@{{orderDetail.code}}</strong><br>
                                        <strong>@{{orderDetail.name}}</strong>
                                    </td>
                                    <td class="text-right">
                                        <input type="hidden" name="mrp[]" value="@{{orderDetail.mrp}}">
                                        <strong>@{{orderDetail.mrp}}</strong>
                                    </td>
                                    <td class="text-right">
                                        <input type="number" min="0" 
                                           ng-min="0"
                                           ng-model="orderDetail.qty"
                                           name="qty[]" 
                                           step="1" class="form-control qty" ng-change="calculateOrder()">
                                    </td>
                                    <td class="text-right">
                                        <span class="row_amount">
                                            @{{orderDetail.totalAmount|number:2}}
                                            <input type="hidden" name="line_total_amount[]" value="@{{orderDetail.totalAmount}}">
                                        </span>
                                    </td>
                                    <td class="text-center" class="text-center">
                                        <a href="" ng-click="removeItem($index)" class="rem-item"><i class="fa fa-minus-circle fa-lg" aria-hidden="true"></i></a>
                                        <a  data-toggle="modal" href='#modal-items' ng-click="getItems(1,'')" class="add-item"><i class="fa fa-plus-circle fa-lg" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4" align="right"><strong>Total Amount</strong></td>
                                    <td align="right">@{{ (order_details|total:'totalAmount')|number:2 }}</td>
                                    <td></td>
                                </tr>                        
                            </tfoot>
                        </table> 
                    </div>

                    <div class="alert alert-error" style="display: none;" id="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>Error!</strong> <h6 id="message"></h6>
                    </div>

                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-12" style="margin-top: 10px;">
                            <div class="form-group">
                                <label>Reference No</label>
                                <input type="text" id="reference_no" name="reference_no" class="form-control" placeholder="Enter Reference No">
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-12" style="margin-top: 10px;">
                            <div class="form-group">
                                <label>Remark</label>
                                <input type="text" id="remark" name="remark" class="form-control" placeholder="Enter Remark">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right" style="margin-top: 10px;">
                            <button type="submit" class="btn btn-primary" ng-disabled="form.$invalid">Save Order</button>
                        </div>
                    </div>

                </div>
            </div>

            <div class="modal fade" id="modal-items">
                <div class="modal-dialog modal-lg">
                    <div class="overlay" id="modal-overlay-2">
                        <i class="fa fa-refresh fa-spin"></i>
                    </div>
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="row">
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <h4 style="margin-top: 5px">Items</h4>
                                </div>
                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                    <div class="input-group">
                                        <input type="text" onchange="getItems(1,'')" name="search" autofocus placeholder="Type Message ..." class="form-control" style="height: 30px">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-defaukt btn-flat btn-sm" ng-click="getItems(items.current_page,search)">Find</button>
                                        </span>
                                    </div>
                                </div>  
                            </div>
                        </div>
                        <div class="modal-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th width="40%">Code</th>
                                        <th width="40%">Name</th>
                                        <th width="18%">Price</th>
                                        <th width="5%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in items.data track by $index" ng-class="isItemInList(item)>0?'item-selected':''">
                                    
                                        <td>@{{item.code}}</td>
                                        <td>@{{item.name}}</td>
                                        <td>@{{item.mrp}}</td>
                                        <td>
                                            <a class="btn btn-sm btn-default" data-dismiss="modal" href="" ng-click="addItem(item)">add</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            
                        </div>
                        <div class="modal-footer">
                            <paging
                                page="items.current_page" 
                                page-size="items.per_page" 
                                total="items.total"
                                show-prev-next="true"
                                show-first-last="true"
                                paging-action="getItems(page,search)">
                              </paging>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </form>

</div>

@stop
@section('js')
<script src="{{asset('assets/angular/angular/angular.min.js')}}"></script>
<script src="{{asset('assets/ng-pagination/dist/paging.min.js')}}"></script>
<script src="{{asset('assets/logics.js')}}"></script>
<script type="text/javascript">

    $(document).ready(function() { 
        $(".chosen").chosen();

        $('.datepick').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            format: 'yyyy-mm-dd'
        });

        $(".btn-group > .btn").click(function(){
            $(".btn-group > .btn").removeClass("active");
            $(this).addClass("active");
        });

        $('.modal-items').on('shown.bs.modal', function() {
            $(this).find('[autofocus]').focus();
        });

        $('#myform').submit(function(event) {
            event.preventDefault(); //this will prevent the default submit

            tmp = 0;

            var customer = $("#customer").val();
            $('#alert').hide();
            if(customer==null || customer==""){
                $('#alert').show();
                $('.alert-error #message').text('Please select customer');
                tmp++;
            }

            if(numberOfItems()==0){
                $('#alert').show();
                $('.alert-error #message').text('You cannot create quotation without items');
                tmp++;
            }; 

            if(isQtyEmpty()){
                $('#alert').show();
                $('.alert-error #message').text('You cannot create quotation without items quantity');
                tmp++;
            };

            if(tmp==0){
                $(this).unbind('submit').submit(); // continue the submit unbind preventDefault
            }
        });

    });

    function formSubmit() {
        // var customer = $("#customer").val();
        // $('#alert').hide();
        // console.log(isQtyEmpty()); 
        // if(customer==null || customer==""){
        //     $('#alert').show();
        //     $('.alert-error #message').text('Please select customer');
        //     return false;
        // }

        // if(numberOfItems()==0){
        //     $('#alert').show();
        //     $('.alert-error #message').text('You cannot create quotation without items');
        //     return false;
        // }; 

        // if(isQtyEmpty()){
        //     $('#alert').show();
        //     $('.alert-error #message').text('You cannot create quotation without items quantity');
        //     return false;
        //     $('#alert').hide();
        // };
        return true;
    }

    function isQtyEmpty() {
        var result = false;
        $('.items-table tbody .item-rows td .qty').each(function (index) {
            var qty = $(this).val();       
            $(this).parent().parent().css( "background-color", "transparent" );
            if(qty==null || qty=="" || qty==0){
                $(this).parent().parent().css( "background-color", "#ffc6c6" );
                result = true;
                return true; 
            }
        });
        return result;
    }

    function numberOfItems() {
        return $('.items-table tbody tr td .qty').length;
    }

    var app = angular.module('app', ['bw.paging']);

    app.filter('total', function(){
        return function(array, type){
            var total = 0;
            angular.forEach(array, function(value, index){
                if(type.indexOf("allTotal") === -1){
                    if(!isNaN(value[type])){
                        total = total + (parseFloat(value[type]));
                    }
                }else{
                    total = total + (parseFloat(value['totalAmount']-value['totalDiscount']+value['totalVat']));
                }
            })
            return total;
        }
    });

    app.controller('AdminOrderController', function AdminOrderController($scope,$filter) {
        
        $scope.items              = [];
        $scope.orderDetails         = [];
        $scope.warehouse          = [];
        $scope.warehouses         = {!!json_encode($warehouses)!!};

        $scope.getItems = function(page,search) {
            $("#modal-overlay-2").show();
            $.ajax({
                url: "{{URL::to('admin/order/getItems')}}",
                method: 'GET',
                data: {'page': page,'search':search},
                async: true,
                success: function (data) {
                    $scope.items = data;
                    $scope.$apply();
                    $("#modal-overlay-2").hide();
                },
                error: function () {
                  
                }
            });
        }

        $scope.calculateOrder = function(){
            
            $scope.order_details = calculateOrderDetails($scope.orderDetails);

        };

        $scope.addItem = function(item) {
            var isItem = $scope.isItemInList(item);
            if(isItem<0){
                $scope.orderDetails.push(item);
                $scope.calculateOrder();
            }else{
            
            }      
        }

        $scope.isItemInList = function (pro) {
            for($i=0;$i<$scope.orderDetails.length;$i++){
                if ($scope.orderDetails[$i].id == pro.id) {
                    return 1;
                } 
            };
            return -1;
        }

        $scope.removeItem = function(index) {
            $scope.orderDetails.splice(index,1);    
        }

        $scope.clearAll = function(item) {
            sweetAlertConfirm("Are you sure ?","This will clear all the items are you sure you want to do this ?",3,function() {
                $scope.orderDetails = [];  
                $scope.$apply(); 
            });         
        }

        $scope.submit = function(){
            $scope.submitted = true;
        }

    });

</script>
@stop
