<?php
namespace App\Modules\AdminOrderManage\Requests;

use App\Http\Requests\Request;

class OrderRequest extends Request
{
    public function authorize(){
        return true;
    }

    public function rules(){
        $rules = [
         //    'customer' => 'required|numeric|min:1',
	        // 'warehouse_id' => 'required|numeric|min:1',
	        // 'qty' => 'required|numeric|min:1'
        ];

        return $rules;
    }

    public function messages(){

        return [
            'customer.min' => 'Please select customer',
            'warehouse_id.min' => 'Please select warehouse'
        ];

    }

}