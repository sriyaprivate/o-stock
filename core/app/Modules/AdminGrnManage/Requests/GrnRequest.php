<?php
namespace App\Modules\AdminGrnManage\Requests;

use App\Http\Requests\Request;

class GrnRequest extends Request
{
    public function authorize(){
        return true;
    }

    public function rules(){
        $rules = [
            'supplier_id' => 'required|numeric|min:1',
	        'warehouse_id' => 'required|numeric|min:1',
	        'qty' => 'required|numeric|min:1'
        ];

        return $rules;
    }

    public function messages(){

        return [
            'supplier_id.min' => 'Please select supplier',
            'warehouse_id.min' => 'Please select warehouse'
        ];

    }

}