<?php namespace App\Modules\AdminGrnManage\Models;

/**
*
* Model
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Baum\Node;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Grn;

class GrnDetail extends Model {

	use SoftDeletes;

	protected $table = 'grn_detail';

	protected $guarded = ['id'];

	public function grn(){
		return $this->belongsTo('App\Modules\AdminGrnManage\Models\Grn', 'grn_id', 'id')->whereNull('deleted_at');
	}

	public function product(){
		return $this->belongsTo('App\Modules\AdminProductManage\Models\Product', 'product_id', 'id')->whereNull('deleted_at');
	}

}