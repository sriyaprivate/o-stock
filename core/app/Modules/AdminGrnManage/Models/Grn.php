<?php namespace App\Modules\AdminGrnManage\Models;

/**
*
* Model
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Baum\Node;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\GrnDetail;

class Grn extends Model{

	use SoftDeletes;

	protected $table = 'grn';

	protected $guarded = ['id'];

	public function details(){
		return $this->hasMany('App\Modules\AdminGrnManage\Models\GrnDetail', 'grn_id')->whereNull('deleted_at');
	}

	public function createdBy(){
		return $this->belongsTo('Core\EmployeeManage\Models\Employee','created_by','id');
	}

	public function supplier(){
		return $this->belongsTo('App\Modules\AdminSupplierManage\Models\Supplier','supplier_id','id');
	}

	public function warehouse(){
		return $this->belongsTo('App\Models\Warehouse','warehouse_id','id');
	}

}
