<?php

Route::group(['middleware' => ['auth']], function()
{   
    Route::group(['prefix'=>'admin/product','namespace' => 'App\Modules\AdminProductManage\Controllers'],function() {

        Route::get('sync', [ 
            'as' => 'product.sync', 'uses' => 'AdminProductManageController@syncProduct'
        ]);

        Route::get('add', [ 
            'as' => 'product.add', 'uses' => 'AdminProductManageController@addView'
        ]);

        Route::get('edit/{id}', [ 
            'as' => 'product.edit', 'uses' => 'AdminProductManageController@editView'
        ]);

        Route::get('list', [ 
            'as' => 'product.list', 'uses' => 'AdminProductManageController@listView'
        ]);

        Route::get('upload', [ 
            'as' => 'product.upload', 'uses' => 'AdminProductManageController@uploadView'
        ]);

        Route::get('delete', [ 
            'as' => 'product.delete', 'uses' => 'AdminProductManageController@delete'
        ]);

        Route::get('stock', [ 
            'as' => 'product.stock', 'uses' => 'AdminProductManageController@stockView'
        ]);

        Route::get('max_discount_upload', [ 
            'as' => 'product.max_discount_upload', 'uses' => 'AdminProductManageController@maxDiscountUploadView'
        ]);

        Route::post('add', [ 
            'as' => 'product.add', 'uses' => 'AdminProductManageController@add'
        ]);

        Route::post('edit/{id}', [ 
            'as' => 'product.edit', 'uses' => 'AdminProductManageController@edit'
        ]);

        Route::post('upload', [ 
            'as' => 'product.upload', 'uses' => 'AdminProductManageController@upload'
        ]);

    });    
});