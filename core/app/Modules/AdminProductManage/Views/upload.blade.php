@extends('layouts.back_master') @section('title','Upload Products')
@section('css')
<style type="text/css">
  
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Product Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/product/list')}}">Product Management</a></li>
            <li class="active">Product Upload</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a href="{{url('admin/product/add')}}" class="btn btn-warning"><i class="fa fa-plus"></i> Create Product</a>
            <button type="button" class="btn btn-danger" onclick="window.location.href='{{url('admin/product/upload')}}'"><i class="fa fa-upload"></i> Products</button>
            <button type="button" class="btn btn-default" onclick="syncProducts()"><i class="fa fa-refresh"></i> Sync</button>
        </div>
    </div>
</div>

<!-- Content-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>List Product</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
            <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                {!!Form::token()!!}

                <div class="form-group @if($errors->has('category')) has-error @endif">
                    <label for="" class="col-sm-2 control-label required">Product Category</label>
                    <div class="col-sm-10">
                        <select name="category" class="form-control chosen">
                            @if(count($categories) > 0)
                                @foreach($categories as $key => $value)
                                    <option value="{{$key}}" @if($key == old('category')) selected @endif>{{$value}}</option>
                                @endforeach
                            @else
                                <option value="">Select a Category</option>
                            @endif
                        </select>
                        @if($errors->has('category'))
                            <span class="help-block">{{$errors->first('category')}}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Download</label>
                    <div class="col-sm-10">
                        <a href="{{asset('assets/excel_formats/product_upload.xls')}}" class="btn btn-warning">
                            <i class="fa fa-download"></i> Sample Excel
                        </a>
                    </div>
                </div>
                <div class="form-group @if($errors->has('file')) has-error @endif">
                    <label for="" class="col-sm-2 control-label required">Browse Excel</label>
                    <div class="col-sm-10">
                        <input type="file" name="file">
                        @if($errors->has('file'))
                            <span class="help-block">{{$errors->first('file')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-success pull-right"><i class="fa fa-floppy-o"></i> Save</button>
                    </div>
                </div>
            </form>

            @if(isset($data))
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-example">
                    <thead>
                        <tr>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $row)
                            <tr>
                                <td width="20%">{{ $row['code'] }}</td>
                                <td>{{ $row['name'] }}</td>
                                <td><span class="{{($row['status'])? 'text-success':'text-danger'}}">{{ $row['message'] }}</span></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @endif
        </div>
    </div>
</div>

@stop
@section('js')
<script type="text/javascript">
    $(document).ready(function() {
        $(".chosen").chosen();
    });
</script>
@stop
