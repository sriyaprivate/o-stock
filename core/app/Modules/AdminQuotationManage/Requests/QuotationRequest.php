<?php
namespace App\Modules\AdminQuotationManage\Requests;

use App\Http\Requests\Request;

class QuotationRequest extends Request
{
    public function authorize(){
        return true;
    }

    public function rules(){
        $rules = [
            'customer_id'   => 'required',
            'delivery_term' => 'required'
        ];

        return $rules;
    }

}