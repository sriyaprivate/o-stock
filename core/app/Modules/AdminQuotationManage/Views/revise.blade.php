@extends('layouts.back_master') @section('title','List Quotation')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<link rel="stylesheet" type="text/css" src="{{asset('assets/dist/bootstrap-datepicker/css/bootstrap-datepicker.css')}}"/>

<style type="text/css">
.pagination {
    display: inline-block;
    padding-left: 0;
     margin: 0 0; 
    border-radius: 4px;
}

.modal .overlay, .overlay-wrapper .overlay {
    z-index: 50;
    background: rgba(255,255,255,0.7);
    border-radius: 3px;
}

.modal .overlay, .overlay-wrapper>.overlay, .box>.loading-img, .overlay-wrapper>.loading-img {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}

.modal  .overlay>.fa, .overlay-wrapper .overlay>.fa {
    position: absolute;
    top: 50%;
    left: 50%;
    margin-left: -15px;
    margin-top: -15px;
    color: #000;
    font-size: 30px;
}

.add-item{
  color: #6aa8e8;
}

.rem-item{
  color: #e45656;
}

[ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
  display: none !important;
}

.item-selected{
  background: #b0d3ff;
}
</style>

@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Quotation<small>Management</small></h1>
  <ol class="breadcrumb">
    <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
    <li><a href="{{ route('quotation.list') }}">Quotation List</a></li>
    <li class="active">create</li>
  </ol>
</section>


<!-- Main content -->
<section class="content" ng-app="app" ng-cloak ng-controller="QuotationController">

    <div class="row">
       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <!-- Default box -->
          <div class="box">
            <div class="box-body">
              @if(isset($quotation) && !empty($quotation))
              <div class="row">
                <div class="col-md-6">
                  <h3 style="margin-top: 2px;margin-bottom: 0px;">{{ $quotation->reference_no?:'-' }}</h3>
                  <h4 style="margin-bottom: 0;margin-top: 8px;">
                    {{ $quotation->customer_name?:'-' }} 
                  </h4>
                  <h5 style="margin-top: 5px;">{{ $quotation->customer_address?:' ' }}</h5>
                  @if($quotation->approved_emp != $quotation->action_emp)
                  <span>Approved By: {{$quotation->approved_emp}}</span>
                  @endif
                  <p style="margin: 0;">Attn : {{ $quotation->attn?$quotation->attn:' ' }}</p>
                  <p style="margin: 0;">{{ $quotation->vat_type?:'-' }} | {{ $quotation->discount_type==1?'Credit':'Cash' }} Quotation </p>
                </div>
                <div class="col-md-6">
                  <h4 class="text-right" style="margin-bottom: 0">
                    {{ ($quotation->quotation_date)?date('jS M, Y',strtotime($quotation->quotation_date)):'-' }}
                  </h4>
                  <h5 class="text-right" style="margin-top:3px;margin-bottom: 0;">
                    {{ $quotation->action_emp?:'-' }}
                  </h5>
                  <h5 class="text-right" style="margin-top: 8px;">
                    @if(PENDING == $quotation->quotation_status)
                      <span class="text-warning">Pending</span>
                    @elseif(APPROVED == $quotation->quotation_status)
                      <span class="text-success">Approved</span>
                    @elseif(REJECTED == $quotation->quotation_status)
                      <span class="text-danger">Rejected</span>
                    @elseif(REVISED == $quotation->quotation_status)
                      <span class="text-primary">Revised</span>
                    @elseif(CLOSED == $quotation->quotation_status)
                      <span class="text-success">Closed</span>
                    @elseif(INVOICED == $quotation->quotation_status)
                      <span class="text-success">Invoiced</span>
                    @else
                      -
                    @endif
                  </h5>
                  @if($quotation->quotation_status != PENDING && $quotation->quotation_status != REJECTED)
                  <div class="text-right">
                    <a href="{{url('admin/quotation/quotation_pdf/')}}/{{$quotation->id}}" target="_blank">Quotation PDF</a> | 
                    <a href="{{url('admin/quotation/performa_pdf/')}}/{{$quotation->id}}" target="_blank">Proforma Invoice PDF</a>
                  </div>
                  @endif
                </div>
              </div>
            @endif
            </div><!-- /.box -->
          </div> 
        </div>
      </div>

  
  <form onsubmit="return formSubmit();" action="{{url('admin/quotation/revise')}}/{{$quotation->id}}" method="post">
  {!!Form::token()!!} 
  <!-- Default box -->
  <div class="box">
    <div class="box-body">      
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <strong>
            <h3 class="text-left hidden-xs hidden-sm" style="margin-top:3px;margin-bottom: 20px;">
              REVISE QUOTATION
            </h3>
            <h3 class="text-center hidden-md hidden-lg" style="margin-top:3px;margin-bottom: 20px;">
              REVISE QUOTATION
            </h3>
          </strong>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-right hidden-xs hidden-sm">

          <button type="button" class="btn btn-md btn-default" ng-click="clearAll()" ng-show="quotDetails.length>0">Clear All</button>
          <button type="button" class="btn btn-md btn-primary" data-toggle="modal" 
          href='#modal-items'          
          ng-click="getItems(1,'')" ng-show="customer!=null">Add Item</button>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center hidden-md hidden-lg">
          
          <div class="row" style="margin-top: 10px;margin-bottom: 10px">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <button type="button" class="btn btn-md btn-default" ng-click="clearAll()" ng-show="quotDetails.length>0">Clear All</button>
              <button type="button" class="btn btn-md btn-primary" data-toggle="modal" 
              href='#modal-items'          
              ng-click="getItems(1,'')" ng-show="customer!=null">Add Item</button>
            </div>
          </div>
        </div>


      </div>
      
      <div class="row">
        <hr style="margin-top:0;margin-bottom: 15px;">
      </div>

      <div class="row hidden-sm hidden-xs">
        <div class="col-md-6">
          <input type="hidden" name="customer_id" value="@{{customer.id}}">
          <input type="hidden" name="vat_type" value="@{{vat_type}}">
          <input type="hidden" name="dis_type" value="@{{dis_type}}">
          <input type="hidden" name="vat_id" value="{{$vat['id']}}">
          <input type="hidden" name="nbt_id" value="{{$nbt['id']}}">
          <input type="hidden" name="vat_per" value="{{$vat['value']}}">
          <input type="hidden" name="inquiry_id" value="0">          
        </div>
      </div>
    
      <div class="container-fluid table-responsive" style="margin-top: 30px;">
        <table class="table table-bordered items-table" width="100%">
          <thead>
            <tr>
              <th class="text-center">#</th>
                <th class="text-center" width="30%">Product / Code</th>
                <th class="text-center" width="10%">Price (Rs)</th>
                <th class="text-center" width="10%">Max (%)</th>
                <th class="text-center" width="10%">
                  <span ng-show="dis_type==='cash'">CASH (%)</span>
                  <span ng-show="dis_type==='credit'">UDP (%)</span>
                </th>
                <th class="text-center" width="20%">Qty</th>
                <th class="text-center" width="20%">Additional (%)</th>
                <th class="text-center" width="20%">Discount (Rs)</th>
                <th class="text-center" width="10%">Amount (Rs)</th>
                <th class="text-center">Actions</th>
            </tr>
          </thead>
          <tbody>
              <tr ng-repeat="quotDetail in quotDetails" class="item-rows">
                    <td>
                      @{{$index+1}}
                      <input type="hidden" name="product_id[]" value="@{{quotDetail.id}}">
                      <input type="hidden" name="is_vat[]" value="@{{quotDetail.is_vat}}">
                      <input type="hidden" name="is_nbt[]" value="@{{quotDetail.is_nbt}}">
                      <input type="hidden" name="vat[]" value="{{$vat['value']}}">
                      <input type="hidden" name="nbt[]" value="{{$nbt['value']}}">
                    </td>
                    <td>
                      <strong>@{{quotDetail.code}}</strong><br>
                      <small>@{{quotDetail.description}}</small>
                    </td>
                    <td align="right">
                      @{{quotDetail.price}}
                      <input type="hidden" name="price[]" value="@{{quotDetail.price}}">
                    </td>
                    <td align="right">
                      @{{quotDetail.max_discount}}
                      <input type="hidden" name="max_discount[]" value="@{{quotDetail.max_discount}}">
                    </td>
                    <td align="right" ng-show="dis_type==='credit'">
                      @{{quotDetail.udp}}
                      <input type="hidden" name="udp[]" value="@{{quotDetail.udp}}">
                    </td> 
                    <td align="right" ng-show="dis_type==='cash'">
                      @{{quotDetail.cash_udp}}
                      <input type="hidden" name="cash_udp[]" value="@{{quotDetail.udp}}">
                    </td>                  
                    <td align="right">
                      <input type="number"
                       ng-min="0"
                       ng-model="quotDetail.qty"
                       name="qty[]" 
                       step="1" class="form-control qty" ng-change="calculateQuotation()">
                    </td>
                    <td align="right">
                      <input type="number" step="0.01" class="form-control" 
                        ng-model="quotDetail.discount" 
                        name="additional_discount[]" 
                        ng-min="0"
                        ng-change="calculateQuotation()" 
                        ng-max="@{{(quotDetail.max_discount - quotDetail.udp)}}" ng-show="dis_type==='credit'">

                      <input type="number" step="0.01" class="form-control" 
                        ng-model="quotDetail.discount" 
                        name="additional_discount[]" 
                        ng-min="0"
                        ng-change="calculateQuotation()" 
                        ng-max="@{{(quotDetail.max_discount - quotDetail.cash_udp)}}" ng-show="dis_type==='cash'">
                    </td>
                    <td align="right">
                       @{{quotDetail.totalDiscount|number:2}}
                      <input type="hidden" name="line_discount_price[]" value="@{{quotDetail.totalDiscount}}">
                    </td>
                    <td align="right">
                      <span class="row_amount">
                        @{{quotDetail.totalAmount|number:2}}
                        <input type="hidden" name="line_total_amount[]" value="@{{quotDetail.totalAmount}}">
                      </span>
                    </td>
                    <td class="text-center">
                      <a href="" ng-click="removeItem($index)" class="rem-item"><i class="fa fa-minus-circle fa-lg" aria-hidden="true"></i></a>
                      <a  data-toggle="modal" href='#modal-items' ng-click="getItems(1,'')" class="add-item"><i class="fa fa-plus-circle fa-lg" aria-hidden="true"></i></a>
                    </td>
              </tr>
              <tr>
                <td colspan="8" align="right">Amount</td>
                <td align="right">@{{ (quotation_details|total:'totalAmount')|number:2 }}</td>
                <td></td>
              </tr>
              <tr ng-hide="quotation_details.length < 1">
                <td colspan="8" align="right">Discount </td>
                <td align="right">@{{ (quotation_details|total:'totalDiscount')|number:2 }}</td>
                <input type="hidden" name="total_discount" value="@{{(quotation_details|total:'totalDiscount')}}">
                <td></td>
              </tr>
              <tr  ng-show="vat_type=='vat'">
                <td colspan="8" align="right">VAT @{{vat}}%</td>
                <td align="right"> @{{ (quotation_details|total:'totalVat')|number:2 }}</td>
                <td></td>
              </tr>
              <tr  ng-show="vat_type=='svat'">
                <td colspan="8" align="right">SVAT @{{vat}}%</td>
                <td align="right">@{{ (quotation_details|total:'totalVat')|number:2 }}</td>
                <td></td>
              </tr>
              <tr>
                <td colspan="8" align="right"><strong>Total Amount</strong></td>
                <td align="right"><strong>@{{ (quotation_details|total:'allTotal')|number:2 }}</strong></td>
                 <input type="hidden" name="total_amount" value="@{{(quotation_details|total:'allTotal')}}">
                 <td></td>
              </tr>
          </tbody>
        </table>
      </div>

      <div class="alert alert-error" style="display: none;" id="alert">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Error!</strong> <h6 id="message"></h6>
      </div>
      

      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right" style="margin-top: 10px;">
          <button type="submit" class="btn btn-primary">Save Quotation</button>
        </div>
      </div>
    </div><!-- /.box-body -->
    <div class="overlay" style="display: none;">
      <i class="fa fa-spin fa-refresh"></i>
    </div>
  </div><!-- /.box -->

  </form>

  <div class="modal fade" id="modal-customer">
    <div class="modal-dialog">
      <div class="overlay" id="modal-overlay-1">
        <i class="fa fa-refresh fa-spin"></i>
      </div>
      <div class="modal-content">
        <div class="modal-header">
         <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <h4 style="margin-top: 5px">Select Customer</h4>
          </div>
          <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
            <div class="input-group">
              <input type="text" name="customerSearch" ng-model="customerSearch" placeholder="Type Message ..." class="form-control" style="height: 30px">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-defaukt btn-flat btn-sm" ng-click="getCustomers(customers.current_page,customerSearch)">Find</button>
                <button type="button" class="btn btn-defaukt btn-flat btn-sm" data-dismiss="modal">Close</button>
              </span>
            </div>
          </div>
         </div>
        </div>
        <div class="modal-body ">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th width="10%">#</th>
                <th width="20%">Code</th>
                <th width="50%">Name</th>
                <th width="10%">Action</th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="customer in customers.data">
                <td>@{{$index+1}}</td>
                <td>@{{customer.short_code}} (@{{customer.vat_type}})</td>
                <td>@{{customer.first_name}} @{{customer.last_name}}</td>
                <td>
                  <a class="btn btn-sm btn-default" href="" data-dismiss="modal" ng-click="selectCustomer(customer)" ng-show="customer.blocked==0">Select</a>
                  <span ng-show="customer.blocked==1">blocked</span>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="modal-footer">
           <paging
              page="customers.current_page" 
              page-size="customers.per_page" 
              total="customers.total"
              show-prev-next="true"
              show-first-last="true"
              paging-action="getCustomers(page,search)">
          </paging> 
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal-items">
    <div class="modal-dialog modal-lg">
      <div class="overlay" id="modal-overlay-2">
        <i class="fa fa-refresh fa-spin"></i>
      </div>
      <div class="modal-content">
        <div class="modal-header">
          <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
              <h4 style="margin-top: 5px">Items</h4>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
              <div class="input-group">
                <input type="text" name="search" ng-model="search" placeholder="Type Message ..." class="form-control" style="height: 30px">
                <span class="input-group-btn">
                  <button type="submit" class="btn btn-defaukt btn-flat btn-sm" ng-click="getItems(items.current_page,search)">Find</button>
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-body">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th width="40%">Code</th>
                <th width="18%">Price</th>
                <th width="11%">UDP (%)</th>
                <th width="11%">CASH (%)</th>
                <th width="11%">MAX (%)</th>
                <th width="5%">Action</th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="item in items.data" ng-class="isItemInList(item)>0?'item-selected':''">
                <td><strong>@{{item.code}}</strong> - <small>@{{item.description}}</small></td>
                <td>@{{item.price}}</td>
                <td>@{{item.udp}}</td>
                <td>@{{item.cash_udp}}</td>
                <td>@{{item.max_discount}}</td>
                <td><a class="btn btn-sm btn-default"  data-dismiss="modal" href="" ng-click="addItem(item)">add</a></td>
              </tr>
            </tbody>
          </table> 
        </div>
        <div class="modal-footer">
          <paging
            page="items.current_page" 
            page-size="items.per_page" 
            total="items.total"
            show-prev-next="true"
            show-first-last="true"
            paging-action="getItems(page,search)">
          </paging> 
        </div>
      </div>
    </div>
  </div>


</section><!-- /.content -->


@stop
@section('js')
<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>
<script src="{{asset('assets/dist/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('assets/dist/angular/angular/angular.min.js')}}"></script>
<script src="{{asset('assets/dist/ng-pagination/dist/paging.min.js')}}"></script>
<script src="{{asset('assets/logics.js')}}"></script>
<script src="{{asset('assets/dist/shortcut/shortcut.js')}}"></script>

<script type="text/javascript">

   function formSubmit() {
    // Validate URL
    $('#alert').hide();

    if(numberOfItems()==0){
      $('#alert').show();
      $('.alert-error #message').text('You cannot create quotation without items');
      return false;
    }; 

    if(isQtyEmpty()){
      return false;
    };    

    return true;

  }

  function isQtyEmpty() {
    var result = false;
    $('.items-table tbody .item-rows td .qty').each(function (index) {
       var qty = $(this).val();       
       $(this).parent().parent().css( "background-color", "transparent" );
       if(qty==null || qty=="" || qty==0){
          $(this).parent().parent().css( "background-color", "#ffc6c6" );
          result = true;
          return true; 
       }
    });
    return result;
  }

  function numberOfItems() {
    return $('.items-table tbody tr td .qty').length;
  }


  var app = angular.module('app', ['bw.paging']);


  app.filter('total', function(){
    return function(array, type){
      var total = 0;
      angular.forEach(array, function(value, index){
        if(type.indexOf("allTotal") === -1){
          if(!isNaN(value[type])){
            total = total + (parseFloat(value[type]));
          }
        }else{
          total = total + (parseFloat(value['totalAmount']-value['totalDiscount']+value['totalVat']));
        }
      })
      return total;
    }
  });


 app.controller('QuotationController', function QuotationController($scope,$filter) {

    $scope.items            = [];
    $scope.quotDetails      = [];
    $scope.quotation        = null;
    $scope.vat_type         = null;
    $scope.dis_type         = null;
    $scope.vat              = '{{$vat["value"]}}';
    $scope.customer         = {!!json_encode($customer)!!};
    $scope.nbt              = '{{$nbt["value"]}}';


    $scope.getQuotationWithDetails = function(quotation_id) {
      $("#modal-overlay-2").show();
      $.ajax({
          url: "{{URL::to('admin/quotation/getQuotationWithDetails')}}",
          method: 'GET',
          data: {'quotation_id': quotation_id},
          async: true,
          success: function (data) {
            $scope.quotDetails = data.data.details;
            $scope.quotation = data.data;
            $scope.vat_type         = data.data.vat_type;
            $scope.dis_type         = data.data.discount_type==1?'credit':'cash';
            $scope.$apply();
            $("#modal-overlay-2").hide();
          },
          error: function () {
              
          }
      });
    }


    $scope.quotation_id             = '{{$quotation->id}}';
    $scope.getQuotationWithDetails($scope.quotation_id);


    $scope.calculateQuotation = function(){
      if($scope.vat_type === 'vat' || $scope.vat_type === 'svat'){
        $scope.quotation_details = calculateVatCustomerQuotation($scope.dis_type,$scope.quotDetails,$scope.vat,$scope.nbt);
      }else if($scope.vat_type === '' || $scope.vat_type === 'notax'){
        $scope.quotation_details = calculateNonVatCustomerQuotation($scope.dis_type,$scope.quotDetails,$scope.vat,$scope.nbt);
      }
    };

    $scope.$watch('vat_type', function (newValue, oldValue) {
      $scope.calculateQuotation();
    });

    $scope.$watch('dis_type', function (newValue, oldValue) {
      $scope.clearDis();
      $scope.calculateQuotation();
    });

    


     


    $scope.getItems = function(page,search) {
      $("#modal-overlay-2").show();
      $.ajax({
          url: "{{URL::to('admin/quotation/getItems')}}",
          method: 'GET',
          data: {'page': page,'search':search},
          async: true,
          success: function (data) {
            $scope.items = data;
            $scope.$apply();
            $("#modal-overlay-2").hide();
          },
          error: function () {
              
          }
      });
    }

    $scope.addItem = function(item) {
      var isItem = $scope.isItemInList(item);
      if(isItem<0){

        $scope.quotDetails.push(item);
      }else{
        //HIGELIGHT RAW

      }      
    }

    $scope.clearAll = function(item) {
      sweetAlertConfirm("Are you sure ?","This will clear all the items are ypu sure you want to do thi ?",3,function() {
        $scope.quotDetails = [];  
        $scope.$apply(); 
      });         
    }

    $scope.removeItem = function(index) {
        $scope.quotDetails.splice(index,1);    
    }

    $scope.isItemInList = function (pro) {
      for($i=0;$i<$scope.quotDetails.length;$i++){
          if ($scope.quotDetails[$i].id == pro.id) {
              return 1;
          } 
      };
      return -1;
    }

    $scope.clearDis = function () {
      $.each($scope.quotDetails, function(index,value) {
        value.discount = 0;
      });    
    }



    shortcut.add("alt+a",function() {
        if($scope.customer!=null){
          $('#modal-items').modal('show');
          $scope.getItems(1,'');
        }else{
          sweetAlertConfirm("No Customer!","Before Add items you have to select customer would you like to select cutomer?",3,function() {
              $('#modal-customer').modal('show');
              $scope.getCustomers(1,'');
          });         
        }
    });


 });


$(document).ready(function() { 
  $(".chosen").chosen();

  $('.datepick').datepicker({
      keyboardNavigation: false,
      forceParse: false,
      format: 'yyyy-mm-dd'
  });

  $(".btn-group > .btn").click(function(){
      $(".btn-group > .btn").removeClass("active");
      $(this).addClass("active");
  });
});
</script>
@stop
