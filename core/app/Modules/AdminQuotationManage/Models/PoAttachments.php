<?php namespace App\Modules\AdminQuotationManage\Models;

/**
*
* Model
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PoAttachments extends Model {

	protected $table = 'po_attachments';

	protected $guarded = ['id'];


}
