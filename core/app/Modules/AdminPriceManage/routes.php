<?php

Route::group(['middleware' => ['auth']], function()
{   
    Route::group(['prefix'=>'admin/price','namespace' => 'App\Modules\AdminPriceManage\Controllers'],function() {

        Route::get('add', [
            'as' => 'price.add', 'uses' => 'AdminPriceManageController@addView'
        ]);

        Route::get('list', [
            'as' => 'price.list','uses' => 'AdminPriceManageController@listView'
        ]);

        Route::get('view/{id}', [
            'as' => 'price.view','uses' => 'AdminPriceManageController@view'
        ]);

        Route::get('edit/{id}', [
            'as' => 'price.edit','uses' => 'AdminPriceManageController@editView'
        ]);

        Route::get('assign/{id}', [
            'as' => 'price.assign','uses' => 'AdminPriceManageController@assignView'
        ]);


        Route::post('add', [
            'as' => 'price.add','uses' => 'AdminPriceManageController@add'
        ]);

        Route::post('assign/{id}', [
            'as' => 'price.assign','uses' => 'AdminPriceManageController@assign'
        ]);

        Route::post('edit/{id}', [
            'as' => 'price.edit','uses' => 'AdminPriceManageController@edit'
        ]);


    });    
});