<?php namespace App\Modules\AdminPriceManage\Controllers;


/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Modules\AdminPriceManage\BusinessLogics\PriceBookLogic;

class AdminPriceManageController extends Controller {

	private $priceLogic;

	public function __construct(PriceBookLogic $priceLogic){
		$this->priceLogic = $priceLogic;
	}

	public function addView()
	{
		return view("AdminPriceManage::add");
	}

	public function add(Request $request)
	{
		$this->validate($request, [
			'code' => 'required|unique:pricebook,code,NULL,id,deleted_at,NULL',
			'name' => 'required',
			'status' => 'required',
			'file' => 'required|mimes:xls',
		]);

		set_time_limit(0);

		try{
			$data = $this->priceLogic->add($request);
		}catch(\Exception $e){
			return redirect('admin/price/add')->with([
				'error' => true,
				'error.title' => 'Error Occurred',
				'error.message' => $e->getMessage()
			]);
		}

		return redirect('admin/price/add')->with([
			'data' => $data,
			'success' => true,
			'success.title' => 'Success',
			'success.message' => 'Successfully added.'
		]);
	}

	public function editView($id)
	{
		$book = $this->priceLogic->getPriceBookById($id);

		return view("AdminPriceManage::edit")->with([ 'data' => $book]);
	}

	public function edit($id, Request $request)
	{
		$this->validate($request, [
			'code' => 'required|unique:pricebook,code,'.$id.',id,deleted_at,NULL',
			'name' => 'required',
			'status' => 'required',
			'file' => 'required|mimes:xls'
		]);

		set_time_limit(0);

		try{
			$book = $this->priceLogic->updatePriceBook($id, $request);
		}catch(\Exception $e){
			return redirect('admin/price/edit/'.$id)->with([
				'error' => true,
				'error.title' => 'Error Occurred',
				'error.message' => $e->getMessage()
			]);
		}

		return redirect('admin/price/edit/'.$id)->with([
			'data' => $book,
			'success' => true,
			'success.title' => 'Success',
			'success.message' => 'Successfully updated.'
		]);
	}

	public function listView(Request $request)
	{
		$old = $request;

		$data = $this->priceLogic->getRecords($old);

		return view("AdminPriceManage::list")->with(compact('old', 'data'));
	}

	public function view($id)
	{
		//$data = $this->priceLogic->getRecords($old);
		$data = $this->priceLogic->getPriceBookById($id);

		return view("AdminPriceManage::view")->with(compact('data'));
	}

	public function assignView($id, Request $request)
	{
		//$data = $this->priceLogic->getRecords($old);
		$old = $request;

		$data = $this->priceLogic->getPriceBookById($id);
		$customers = $this->customerLogic->getCustomersWithPriceBook($id, true, true, $request);

		return view("AdminPriceManage::assign")->with(compact('data', 'customers', 'old'));
	}

	public function assign($id, Request $request)
	{
		//$data = $this->priceLogic->getRecords($old);
		try{
			$data = $this->priceLogic->assignCustomer($id, $request);
		}catch(\Exception $e){
			$data = [
				'status' => 0,
				'message' => $e->getMessage()
			];
		}

		return response()->json($data);
	}
}
