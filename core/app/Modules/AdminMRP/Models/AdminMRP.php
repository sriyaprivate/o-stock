<?php namespace App\Modules\AdminMRP\Models;

/**
*
* Model
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminMRP extends Model {

	/**
     * table row delete
     */
    use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'mrp';

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

	/**
	 * product
	 * @return object product
	 */
	public function product(){
        return $this->belongsTo('App\Modules\AdminProductManage\Models\Product', 'product_id','id');
    }

}
