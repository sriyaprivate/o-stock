<?php
namespace App\Modules\AdminProductCategory\Requests;

use App\Http\Requests\Request;

class ProductCategoryRequest extends Request {

	public function authorize(){
		return true;
	}

	public function rules(){
		$id = $this->id;

		if($this->is('admin/product-category/create')){
			$rules = [
				'name'	=> 'required|unique:product_category,name',
				'code'	=> 'required|unique:product_category,code'
				
			];
		}else if($this->is('admin/product-category/edit/'.$id)){
			$rules = [
				'name'	=> 'required|unique:product_category,name,'.$id,
				'code'	=> 'required|unique:product_category,code,'.$id,
				'main_category'	=> 'required|numeric|min:1,main_category_id'.$id
				
			];
		}

		return $rules;
	}

	public function messages(){

        return [
            'main_category.min' => 'The Main category can not be blank'
        ];

    }

}
