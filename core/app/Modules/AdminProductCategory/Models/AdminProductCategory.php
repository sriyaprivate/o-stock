<?php namespace App\Modules\AdminProductCategory\Models;

/**
*
* Model
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminProductCategory extends Model{

	/**
     * table row delete
     */
    use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'product_category';

	// guard attributes from mass-assignment
	protected $guarded = array('id');

	/**
	 * Parent product category
	 * @return object parent product category
	 */
	public function mainCategory() {
		return $this->belongsTo('App\Models\MainCategory', 'main_category_id', 'id');
	}

}
