@extends('layouts.back_master') @section('title','Admin - Product Category Management') 
@section('css')
<style type="text/css">
    
</style>
@stop

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Product Category Management</h2>
        <ol class="breadcrumb">
            <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{url('admin/product-category/index')}}">Product Category</a></li>
            <li class="active">List Product Category</li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a href="{{url('admin/product-category/create')}}" class="btn btn-warning"><i class="fa fa-plus"></i> Add Category</a>
        </div>
    </div>
</div>

<!-- Content-->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>List Product Category</h5>
            <div class="ibox-tools">
                
            </div>
        </div>

        <div class="ibox-content" style="display: block;">
            <div class="">
                <table class="table table-striped table-bordered table-hover dataTables-example">
                    <thead>
                        <form id="form" role="form" method="get" action="{{url('admin/product-category/search')}}">
                            <tr>
                                <th width="5%">#</th>
                                <th>Category Code </br>
                                    <input type="text" class="form-control" name="code" placeholder="Enter Code" value="{{$code}}">
                                </th>
                                <th>Category Name </br>
                                    <input type="text" class="form-control" name="name" placeholder="Enter Name" value="{{$name}}">
                                </th>
                                <th>Main Category </br>
                                    {!! Form::select('parent', $main_categories, $parent,['class'=>'chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose Parent','id'=>'parent']) !!}
                                </th>
                                <th>Created At</th>
                                <th width="10%">Action <br>
                                    <button type="submit" class="btn btn-default btn-sm" id="plan"><i class="fa fa-search" style="padding-right: 16px;width: 28px;"></i>Find</button>
                                </th>
                            </tr>
                        </form>
                    </thead>
                    <tbody>
                    <?php $i = 1;?>
                        @if(count($product_category_details) > 0)
                            @foreach($product_category_details as $result_val)
                                @include('AdminProductCategory::template.category')
                                <?php $i++;?>
                            @endforeach
                        @else
                            <tr><td colspan="6" class="text-center">- No data found -</td></tr>
                        @endif
                    </tbody>
                </table>
                @if($product_category_details != null)
                    <div style="float: right;">{!! $product_category_details->render() !!}</div>
                @endif
            </div>
        </div>
    </div>
</div>

@stop

@section('js')
<script type="text/javascript">
    $(document).ready(function() {
        $(".chosen").chosen();

        $('#parent').change(function(){
            $("form").submit();
        });
    });
</script>
@stop
