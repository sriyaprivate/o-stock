@extends('layouts.back_master') @section('title','Admin - MRP Management')
@section('current_title','Product Category Create')

@section('css')
<style type="text/css">
  .box-header, .box-body {
    padding: 20px;
  }
  .has-error .help-block, .has-error .control-label{
    color:#e41212;
  }
  .has-error .chosen-container{
    border:1px solid #e41212;
  }
</style>
@stop

@section('content')
<!-- Content-->
<section>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Bank<small>Management</small></h1>
    <ol class="breadcrumb">
      	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      	<li>banks</li>
      	<li class="active">list</li>
    </ol>
  </section>
  <!-- !!Content Header (Page header) -->

  <!-- Main content -->
  <section class="content">

    <div class="box">
      <div class="box-body">
         <form method="get">
            <div class="row form-group">
              <div class="col-sm-5">
                <label for="" class="control-label">Bank Name</label>
                <input type="text" class="form-control" name="bank_name" id="reference" value="{{$request->bank_name}}">
              </div>
              <div class="col-sm-4">
                <label for="" class="control-label">Code</label>
                <input type="text" class="form-control" name="code" id="customer" value="{{$request->code}}">
              </div>
              <div class="col-sm-3 text-right">
                 <div style="margin-top: 21px;">
                   <button type="submit" class="btn btn-default btn-sm" id="plan"><i class="fa fa-search" 
                  style="padding-right: 16px;width: 28px;"></i>Find</button>
                  <a href="list" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top">
                    <i class="fa fa-refresh" style="padding-right: 16px;width: 20px;"></i>Refresh</a>   
                 </div>           
              </div>
            </div>
          </form>
      </div>
    </div>

    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Bank List</h3>
      </div>
      <div class="box-body">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th width="5%">#</th>
              <th>Name</th>
              <th>Code</th>
              <th width="10%" class="text-center">Actions</th>
            </tr>
          </thead>
          <tbody>
            @foreach($banks as $key=>$bank)
            <tr>
              <td>{{$key+1}}</td>
              <td>{{$bank->bank_name}}</td>
              <td>{{$bank->short_code}}</td>
              <td class="text-center">
                <div class="btn-group">
                    @if($user->hasAnyAccess(['admin','bank.delete']))
                    <a class="btn btn-xs btn-default delete" data-toggle="tooltip" data-placement="top" title="Delete Bank" onclick="deleteData('{{url('admin/bank/delete/'.$bank->id)}}');"><i class="fa fa-trash-o"></i></a>
                    @endif
                    @if($user->hasAnyAccess(['admin','bank.edit']))
                    <a href="{{url('admin/bank/edit/')}}/{{$bank->id}}" class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="Edit Bank"><i class="fa fa-pencil"></i></a>
                    @endif
                </div>                
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>

  </section>
  <!-- !!Main content -->

</section>
<!-- !!!Content -->

@stop
@section('js')  
  <!-- CORE JS -->
	<script type="text/javascript">
    	function deleteData(_url){
          swal({
            title: "Are you sure?",
            text: "you wanna to delete this bank?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
          },
          function(){
            $.ajax({
              url: _url,
              method: 'get',
              cache: false,
              data: [],
              success: function(response){
                if(response.status == 1){
                  swal("Done!", "bank has been deleted!.", "success");
                  location.reload();
                }else{
                  swal("Error!", response.message, "error");
                }
              },
              error: function(xhr){
                console.log(xhr);
                swal("Error!", 'Error occurred. Please try again', "error");
              } 
            });
            //window.open(_url, '_self');
          });
        }
	</script>
  <!-- //CORE JS -->
@stop
