@extends('layouts.back_master') @section('title','Edit Supplier')
@section('css')
<style type="text/css">
  
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Supplier 
    <small> Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
        <li><a href="{{ url('/admin/type/supplier/') }}">Supplier</a></li>
        <li class="active">Add</li>
    </ol>
</section>
<!-- SEARCH -->
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Supplier Add</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-warning btn-xs" onclick="goBack()"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button>
            </div>
        </div>
        <div class="box-body">
            <form action="{{ route('admin.supplier.update', $supplier->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" class="form-horizontal" method="post">
                {{ csrf_field() }}
                @include ('supplier::supplier.form', ['submitButtonText' => 'Update', 'page' => 'edit'])
            </form>
        </div>
    </div>  
</section>
@stop
@section('js')

<script type="text/javascript">
$(document).ready(function() {
  
});
</script>
@stop




































