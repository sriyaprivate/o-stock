

@extends('layouts.back_master') @section('title','Show Supplier')
@section('css')
<style type="text/css">
  
    
</style>
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Supplier 
    <small> Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
        <li><a href="#">Supplier</a></li>
        <li class="active">Show</li>
    </ol>
</section>

<!-- SEARCH -->
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Supplier - {{ $supplier->name }}</h3>
            <div class="box-title pull-right">
                <button type="button" onclick="goBack()" class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button>
                <a href="{{ route('admin.supplier.edit', $supplier->id) }}" title="Edit Supplier"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                <button type="button" class="btn btn-danger btn-xs delete-btn" data-url="{{ route('admin.supplier.destroy', $supplier->id) }}">
                    <i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                </button>
            </div>
        </div>
        <div class="box-body">
            <!-- <a href="{{ route('admin.supplier.destroy', $supplier->id) }}" class="btn btn-danger btn-xs">
                <i class="fa fa-trash-o" aria-hidden="true"></i> Delete
            </a> -->
            <br/>
            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th width="20%">ID</th>
                            <td>{{ $supplier->id?:'-' }}</td>
                        </tr>
                        <tr>
                            <th>Name</th>
                            <td>{{ $supplier->name?:'-' }}</td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>{{ $supplier->email?:'-' }}</td>
                        </tr>
                        <tr>
                            <th>Company</th>
                            <td>{{ $supplier->company?:'-' }}</td>
                        </tr>
                        <tr>
                            <th>Contact</th>
                            <td>{{ $supplier->contact_first?:'-' }}</td>
                        </tr>
                        <tr>
                            <th>Other Contact</th>
                            <td>{{ $supplier->contact_second?:'-' }}</td>
                        </tr>
                        <tr>
                            <th>Address</th>
                            <td>{{ $supplier->address?:'-' }}</td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td>
                                @if($supplier->status == 1)
                                    <span class="fa fa-check-circle lbl-green"></span>
                                @else
                                    <span class="fa fa-check-circle lbl-gray"></span>
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>  
</section>



@stop
@section('js')

<script type="text/javascript">
$(document).ready(function() {
    $('.delete-btn').click(function(){    
        var link    = $(this).data('url');
        var title   = 'Confirm!.';
        var message = 'Are you sure you want to delete this supplier?.'

        confirm_delete(link, title, message);
    });
});
</script>
@stop




























