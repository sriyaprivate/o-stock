
@extends('layouts.back_master') @section('title','Add new supplier')
@section('css')
<style type="text/css">
  
    
</style>
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Supplier <small> Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('/') }}"><i class="fa fa-home mr5"></i>Home</a></li>
        <li><a href="{{ route('admin.supplier.index') }}">Supplier</a></li>
        <li class="active">Add</li>
    </ol>
</section>
<!-- SEARCH -->
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Supplier Add</h3>
            <div class="box-title pull-right">
                <button onclick="goBack()" class="btn btn-warning btn-sm" title="Go Back">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                </button>
            </div>
        </div>
        <div class="box-body">
            <form method="post" action="{{ route('admin.supplier.store') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                {{ csrf_field() }}
                @include ('supplier::supplier.form', ['page' => 'create'])
            </form>
        </div>
    </div>  
</section>

@stop
@section('js')

<script type="text/javascript">
$(document).ready(function() {
  
});
</script>
@stop




































