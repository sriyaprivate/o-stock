@extends('layouts.back_master') @section('title','List Supplier')
@section('css')
<style type="text/css">
  
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Supplier 
    <small> Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
        <li class="active">Supplier</li>
    </ol>
</section>
<!-- SEARCH -->
<section class="content">
    <!-- Default box -->
    <div class="box">        
        <div class="box-body">
            <form action="{{ route('admin.supplier.index') }}" method="get">
                <div class="row">
                    <div class="col-lg-3">
                        <label class="control-label">Search</label>
                        <select class="form-control chosen" name="status">
                            @if(Request::get('status') == null)
                                <option value="" selected>All</option>
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            @elseif(Request::get('status') == 1)
                                <option value="">All</option>
                                <option value="1" selected>Active</option>
                                <option value="0">Inactive</option>
                            @elseif(Request::get('status') == 0)
                                <option value="">All</option>
                                <option value="1">Active</option>
                                <option value="0" selected>Inactive</option>
                            @else
                                <option value="" selected>All</option>
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            @endif
                        </select>
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">&nbsp;</label>
                        <div class="input-group">
                            <input type="text" name="search" class="form-control" placeholder="Search for..." value="{{ empty(Input::get('search'))? old('search') : Input::get('search') }}">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-default">Search</button>
                            </span>
                        </div>
                        <br>
                    </div>
                </div>
            </form>
        </div>
    </div> 

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Supplier</h3>
            <div class="box-title pull-right">
                <a href="{{ route('admin.supplier.create') }}" class="btn btn-success btn-sm" title="Add New Supplier">
                    <i class="fa fa-plus" aria-hidden="true"></i> Add New
                </a>
            </div>
        </div>
        <div class="box-body">
            <div class="table-responsive" style="overflow: scroll;">
                <table class="table table-bordered bordered table-striped table-condensed">
                    <thead>
                        <tr>
                            <th style="text-align:center">#</th>
                            <th style="text-align:center">Code</th>
                            <th style="text-align:center">Name</th>
                            <th style="text-align:center">Email</th>
                            <th style="text-align:center">Address</th>
                            <th style="text-align:center">Company</th>
                            <th style="text-align:center">Contact</th>
                            <th style="text-align:center">Contact-2</th>
                            <th style="text-align:center">Date</th>
                            <th style="text-align:center">Status</th>
                            <th style="text-align:center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($supplier) > 0)
                        @foreach($supplier as $key => $item)
                            <tr>
                                <td style="text-align:center">{{ (++$key) }}</td>
                                <td style="text-align:center">{{ $item->code?:'-' }}</td>
                                <td style="text-align:center">{{ $item->name?:'-' }}</td>
                                <td style="text-align:center">{{ $item->email?:'-' }}</td>
                                <td style="text-align:center">{{ $item->address?:'-' }}</td>
                                <td style="text-align:center">{{ $item->company?:'-' }}</td>
                                <td style="text-align:center">{{ $item->contact_first?:'-' }}</td>
                                <td style="text-align:center">{{ $item->contact_second?:'-' }}</td>
                                <td style="text-align:center">{{ $item->created_at->format('Y-m-d')?:'-' }}</td>
                                <td style="text-align:center">
                                    @if($item->status == 1)
                                        <span class="fa fa-check-circle lbl-green"></span>
                                    @else
                                        <span class="fa fa-check-circle lbl-gray"></span>
                                    @endif
                                </td>
                                <td style="text-align:center;min-width: 200px;">
                                    <a href="{{ route('admin.supplier.show', $item->id) }}" title="View Supplier">
                                        <button class="btn btn-info btn-xs">
                                            <i class="fa fa-eye" aria-hidden="true"></i> View
                                        </button>
                                    </a>
                                    <a href="{{ route('admin.supplier.edit', $item->id) }}" title="Edit Supplier">
                                        <button class="btn btn-primary btn-xs">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                                        </button>
                                    </a>
                                    <button type="button" class="btn btn-danger btn-xs delete-btn" data-url="{{ route('admin.supplier.destroy', $item->id) }}">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <td colspan="10">
                            <h2 align="center" class="lbl-gray">Data not found!</h2>
                        </td>
                    @endif
                    </tbody>
                </table>
                <div class="pagination-wrapper pull-right"> {!! $supplier->appends(['search' => Request::get('search'), 'status' => Request::get('status')])->render() !!} </div>
            </div>            
        </div>
    </div>  
</section>
@stop
@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('.delete-btn').click(function(){    
        var link    = $(this).data('url');
        var title   = 'Confirm!.';
        var message = 'Are you sure you want to delete this supplier?.'

        confirm_delete(link, title, message);
    });
});
</script>
@stop


































