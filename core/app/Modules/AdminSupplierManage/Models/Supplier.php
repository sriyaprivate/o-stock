<?php

namespace App\Modules\AdminSupplierManage\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'supplier';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    protected $timestamp = true;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'company', 
        'address', 
        'email', 
        'contact_first',
        'contact_second', 
        'status', 
        'created_at', 
        'updated_at'
    ];

    
}
