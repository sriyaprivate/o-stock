<?php

Route::group(['middleware' => ['auth']], function(){
  
    Route::group(['prefix' => 'admin/tax', 'namespace' => 'App\Modules\AdminTaxManage\Controllers'],function(){

        /*** GET Routes*/
        Route::get('add', [
            'as' => 'tax.add', 'uses' => 'TaxController@addView'
        ]);

        /*** POST Routes*/
        Route::post('add', [
            'as' => 'tax.add', 'uses' => 'TaxController@add'
        ]);

    });
    
});