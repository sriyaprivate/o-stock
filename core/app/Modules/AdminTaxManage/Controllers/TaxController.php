<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 12/10/2015
 * Time: 10:23 AM
 */

namespace App\Modules\AdminTaxManage\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\AdminTaxManage\Requests\TaxRequest;
use App\Modules\AdminTaxManage\Models\Tax;
use Response;
use Sentinel;

class TaxController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    /**
     * Show the location type add screen to the user.
     *
     * @return Response
     */
    public function addView()
    {
        $vat = Tax::whereNull('ended_at')->first();
        return view('AdminTaxManage::add')->with(['vat' => $vat]);
    }

    /**
     * add new location type data to database
     * @param $request
     * @return Redirect to type add
     */
    public function add(TaxRequest $request)
    {
        $vat_value = $request->get('vat_value');
        $tax = Tax::where('value',$vat_value)->whereNull('ended_at')->first();
        if(!is_object($tax)){
            $tax = Tax::whereNull('ended_at')->first();
            if(is_object($tax)){
                $tax->ended_at = date('Y-m-d H:i:s');
                $tax->save();
            }
            Tax::create(['value'=>$vat_value]);
        }
        return redirect()->back()->with(['success' => true,
            'success.message' => 'Tax Update successfully!',
            'success.title' => 'Success..!']);

    }
}