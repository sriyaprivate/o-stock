<?php


Route::group(array('prefix'=>'test','module' => 'PaymentManage', 'namespace' => 'App\Modules\PaymentManage\Controllers'), function() {

    Route::resource('PaymentManage', 'PaymentManageController');
    
}); 