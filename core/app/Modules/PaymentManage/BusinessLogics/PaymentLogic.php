<?php namespace App\Modules\PaymentManage\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Modules\PaymentManage\Models\OrelPayTransaction;
use App\Modules\PaymentManage\Models\Payment;

class PaymentLogic {

	

	public function orelPayTransaction($order,$transaction_no,$status,$message)
	{
		return OrelPayTransaction::create([
			'user_id'        =>  $order->order_initiate_by,
			'user_type'      =>  $order->initiate_user_type,
			'order_no'       =>  $order->reference_no,
			'transaction_no' =>  $transaction_no,
			'status'         =>  $status,
			'message'        =>  $message
		]);
	}

	public function addNewPayment($payment_value,$order){

		return Payment::create([
			'payment_value'      =>  $payment_value,
			'order_no'           =>  $order->reference_no,
			'order_id'           =>  $order->id,
			'order_initiated_by' =>  $order->order_initiate_by,
			'initiate_user_type'  => $order->initiate_user_type
		]);
	}

}
