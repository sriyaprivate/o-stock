<?php namespace App\Modules\ERPDataManage\Controllers;


/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\ERPDataManage\Models\ERPDataManage;
use App\Modules\ERPDataManage\Models\SyncStatus;
use App\Modules\ERPDataManage\Models\SyncType;
use App\Modules\AdminProductManage\Models\Product;
use App\Modules\AdminProductCategory\Models\AdminProductCategory as Category;
use App\Modules\AdminCustomerManage\Models\Customer;
use Illuminate\Http\Request;
use App\Modules\ERPDataManage\BusinessLogics\ERPLogic;
use App\Modules\AdminMRP\BusinessLogics\MRPLogic;
use App\Models\Quotation;
use App\Models\Invoice;
use App\Models\InvoiceDetails;
use App\Models\SyncTime;
use Sentinel;
use DB;

class ERPDataManageController extends Controller {

	protected $erpLogic;
	protected $mrpLogic;

	public function __construct(ERPLogic $erpLogic, MRPLogic $mrpLogic)
	{
		$this->erpLogic = $erpLogic;
		$this->mrpLogic = $mrpLogic;
	}

	public function cleanString($string){
		$string = preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&\-+]:/s', '',$string);
		return trim($string);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return $this->erpLogic->getProjectCustomers();
		return view("ERPDataManage::index");
	}


	public function listView()
	{
		$sync_type = DB::select(DB::raw(
								"SELECT 
								    st.id,
								    st.name,
								    CONCAT(e.first_name, ' ', e.last_name) AS employee,
								    tmp.status,
								    tmp.last_sync
								FROM
								    sync_type st
								        LEFT JOIN
								    (SELECT 
								        ss.id,
								            ss.employee_id,
								            ss.data,
								            ss.sync_type_id,
								            ss.status,
								            ss.created_at AS last_sync
								    FROM
								        sync_status ss
								    INNER JOIN (SELECT 
								        MAX(id) AS id
								    FROM
								        sync_status
								    GROUP BY sync_type_id) tmp ON ss.id = tmp.id) tmp ON st.id = tmp.sync_type_id
								        LEFT JOIN
								    employee e ON e.id = tmp.employee_id
								WHERE
								    st.status = 1"
							));
        return view("ERPDataManage::list")->with(['sync_type'=>$sync_type]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

    public function create(Request $request)
    {
    	set_time_limit(0);
    	ini_set('max_execution_time', 0);
    	ini_set('max_input_time', -1);
		$user = Sentinel::getUser();

		if(!$user)
			$user = (object)['id' => 1, 'employee_id' => 1];

	    $type = $request->type;
      
        try {
            // DB::beginTransaction();
                
            $status = 1;
            if ($type == 1) {
            	$proudctPages = $this->erpLogic->getProductPageCount(2000);
            	for($i = 1; $i <= $proudctPages; $i++){
	            	$data = $this->getAllProduct($i);

		        	if(sizeof($data)){
		        		$pros = [];
		            	foreach ($data as $product) {
		            		$product->t_citg = $this->cleanString($product->t_citg);
		            		$product->t_item = $this->cleanString($product->t_item);
		            		$product->t_dsca = $this->cleanString($product->t_dsca);
		            		$product->t_cvat = $this->cleanString($product->t_cvat);
		            		$product->t_cuni = $this->cleanString($product->t_cuni);
		            		$product->item_group = $this->cleanString($product->item_group);
		            		$product->item_pillar = $this->cleanString($product->item_pillar);

		            		$product_id = trim($product->t_item);
		            		$product_name = $product->t_dsca;
		            		$tax = $product->t_cvat;
		            		$unit = $product->t_cuni;
		            		$is_vat = 0;
		            		$is_nbt = 0;

		            		if(strpos($tax, 'NBT') !== FALSE){
		            			$is_nbt = 1;
		            		}

		            		if(strpos($tax, 'VAT') !== FALSE){
		            			$is_vat = 1;
		            		}

		            		$category = Category::where('code', $product->t_citg)->first();

		            		if(!$category){
		            			$category = Category::create([
		            				'name' => $product->item_group,
		            				'code' => $product->t_citg,
		            				'timestamp' => date('Y-m-d H:i:s')
		            			]);
		            		}elseif($category->name != $product->item_group){
		            			$category->name = $product->item_group;
		            			$category->timestamp = date('Y-m-d H:i:s');
		            			$category->save();
		            		}

			                $pro  = Product::where('code','=', $product_id)->first();
		                    if ($pro){
		                    	if($pro->name != $product_name || 
		                    			$pro->description != $product_name || 
		                    			$pro->is_vat != $is_vat || 
		                    			$pro->is_nbt != $is_nbt || 
		                    			$pro->vat_code != $tax || 
		                    			$pro->unit != $unit || 
		                    			$pro->product_vertical != $product->item_pillar || 
		                    			$pro->product_category_id != $category->id){
			                        $pro->name = $product_name;
			                        $pro->description = $product_name;
			                        $pro->is_vat = $is_vat;
			                        $pro->is_nbt = $is_nbt;
			                        $pro->vat_code = $tax;
			                        $pro->unit = $unit;
			                        $pro->product_vertical = $product->item_pillar;
			                        $pro->product_category_id = $category->id;
			                        $pro->timestamp = date('Y-m-d H:i:s');
			                        $pro->updated_at = date('Y-m-d H:i:s');
			                        $pro->save();
			                    }
		                    } else {
		                        if($product_id){
		                            array_push($pros,[
		                                "code" => $product_id,
		                                "name" => $product_name,
		                                "description" => $product_name,
		                                "is_vat" => $is_vat,
		                                "is_nbt" => $is_nbt,
		                                "vat_code" => $tax,
		                                "unit" => $unit,
		                                "product_vertical" => $product->item_pillar,
		                                'product_category_id' => $category->id,
		                                "created_at" => date('Y-m-d H:i:s'),
		                                "updated_at" => date('Y-m-d H:i:s'),
		                                "timestamp" => date('Y-m-d H:i:s')
		                            ]);
		                        }
		                    }
		                }

		                if(count($pros) > 0){
		                	$products = Product::insert($pros);
		                }
		        	}
	        	}
            } elseif ($type == 2) {
            	$customerPages = $this->erpLogic->getCustomerPageCount(2000);

            	for($i=1;$i<=$customerPages;$i++){
            		$data = $this->getAllCustomer($i, 2000);
	            	if(sizeof($data)){
	            		$cust = [];
	                    foreach ($data as $customer) {
	                    	$customer->t_itbp = $this->cleanString($customer->t_itbp);
	                    	$customer->nam = $this->cleanString($customer->nam);
	                    	$customer->Cus_Group = $this->cleanString($customer->Cus_Group);
	                    	$customer->Terms_Payment = $this->cleanString($customer->Terms_Payment);
	                    	$customer->Cr_Limit = $this->cleanString($customer->Cr_Limit);
	                    	$customer->New_Credit_Days = $this->cleanString($customer->New_Credit_Days);

	                    	$bpr_code = trim($customer->t_itbp," ");

	                    	$vvt = ($customer->VAT && $customer->VAT != '')?1:0;
	                    	$ssvt = ($customer->VAT && 
	                            		$customer->VAT != '' && 
	                            		$customer->svt && 
	                            		$customer->svt != '')?'svat':(($customer->VAT && 
	                            		$customer->VAT != '')?'vat':'');

	                        $is_customer = Customer::where('code', $bpr_code)->first();
	                        if ($is_customer){
	                        	if($is_customer->first_name != $customer->nam ||
	                        		$is_customer->customer_group != $customer->Cus_Group ||
	                        		$is_customer->payment_terms != $customer->Terms_Payment ||
	                        		$is_customer->credit_limit != $customer->Cr_Limit ||
	                        		$is_customer->credit_period != $customer->New_Credit_Days ||
	                        		$is_customer->customer_address_code != $customer->t_cadr ||
	                        		$is_customer->is_vat != $vvt ||
	                        		$is_customer->vat != $ssvt
	                        	){
		                            $customer_dtl = $is_customer;
		                            $customer_dtl->first_name = $customer->nam;
		                            $customer_dtl->customer_group = $customer->Cus_Group;
		                            $customer_dtl->payment_terms = $customer->Terms_Payment;
		                            $customer_dtl->credit_limit = $customer->Cr_Limit;
		                            $customer_dtl->credit_period = $customer->New_Credit_Days;
		                            $customer_dtl->is_vat = ($customer->VAT && $customer->VAT != '')?1:0;
		                            $customer_dtl->vat = ($customer->VAT && 
		                            		$customer->VAT != '' && 
		                            		$customer->svt && 
		                            		$customer->svt != '')?'svat':(($customer->VAT && 
		                            		$customer->VAT != '')?'vat':'');
		                            $customer_dtl->customer_address_code = $customer->t_cadr;
		                            $customer_dtl->timestamp = date('Y-m-d H:i:s');

		                            $customer_dtl->save();
		                        }
	                        } else {
	                            if($bpr_code){

	                                array_push($cust,[
	                                	"code" => $bpr_code,
	                                    "first_name" => $customer->nam,
	                                    "customer_group" => $customer->Cus_Group,
	                                    "payment_terms" => $customer->Terms_Payment,
	                                    "credit_limit" => $customer->Cr_Limit,
	                                    "credit_period" => $customer->New_Credit_Days,
	                                    "is_vat" => ($customer->VAT && $customer->VAT != '')?1:0,
	                                    "vat" => ($customer->VAT && 
				                            		$customer->VAT != '' && 
				                            		$customer->svt && 
				                            		$customer->svt != '')?'svat':(($customer->VAT && 
	                            		$customer->VAT != '')?'vat':''),
	                                    "blocked" => 0,
	                                    "status" => 1,
	                                    "customer_address_code" => $customer->t_cadr,
		                                "created_at" => date('Y-m-d H:i:s'),
		                                "updated_at" => date('Y-m-d H:i:s'),
		                                "timestamp" => date('Y-m-d H:i:s')
	                                ]);
	                            }
	                        }
	                    }

	                    $cust = Customer::insert($cust);
	            	}
            	}
            	$block_customers = $this->erpLogic->getBlockStatus();

            	foreach ($block_customers as $key => $value) {
            		$check_customer = Customer::where('code','=',$value->OutletID)->first();
            		if($check_customer && $check_customer->blocked != $value->CreditBlockStatus){
            			$block = $check_customer;
            			$block->blocked = $value->CreditBlockStatus;
            			$block->timestamp = date('Y-m-d H:i:s');
            			$block->save();
            		} 	
            	}

            	$customer_addresses = $this->erpLogic->getCustomerAddresses();

            	foreach ($customer_addresses as $key => $value) {
            		$check_customer = Customer::where('code','=',$value->t_itbp)->first();
            		$addr = $value->t_namc.' '.$value->t_namd.' '.$value->t_dsca;
            		if($check_customer && 
            			($check_customer->address != $addr || 
            				$check_customer->contact_no != $value->t_telp)){

            			$block = $check_customer;
            			$block->address = $value->t_namc.' '.$value->t_namd.' '.$value->t_dsca;
            			$block->contact_no = $value->t_telp;
            			$block->timestamp = date('Y-m-d H:i:s');
            			$block->save();
            		} 	
            	}
            } elseif ($type == 3) {
            	$prices = $this->erpLogic->getPrices();

            	if(count($prices) > 0){
            		foreach($prices as $key => $price){
            			$item_code = $this->cleanString($price->t_item);
            			$mrp_price = number_format($this->cleanString($price->t_bapr),4,'.','');

            			$pro  = Product::where('code','=', $item_code)->first();

            			if($pro){
            				if($pro->mrp != $mrp_price){
	            				$priceData = (object)[
	            					'product_id' => $pro->id, 
	            					'mrp' => $mrp_price, 
	            					'code' => $pro->code
	            				];
	            				$this->mrpLogic->add($priceData, $user);
            				}
            			}

            		}
            	}
            } elseif ($type == 4) {
            	/*$quotations = Quotation::where('status', CLOSED)->get();
            	$currentTimestamp = date('Y-m-d H:i:s');

            	$previousTimestamp = SyncTime::where('name','invoice')->first()->last_sync;

            	if(count($quotations) > 0){
            		foreach($quotations as $key => $quotation){
            			$erpSalesOrders = $this->erpLogic->getInvoices($quotation->reference_no);

            			if($erpSalesOrders && count($erpSalesOrders) > 0){

            				foreach($erpSalesOrders as $erpSalesOrder){
            					$invoice = Invoice::where('sales_order_no', $erpSalesOrder->header->sales_order_no)->first();

	            				if(!$invoice){
	            					$invoice = new Invoice;
	            					$invoice->sales_order_no = $this->cleanString($erpSalesOrder->header->sales_order_no);
	            					$invoice->invoice_no = $this->cleanString($erpSalesOrder->header->invoice_type).'-'.str_replace(',', '', $this->cleanString($erpSalesOrder->header->invoice_document));
	            					$invoice->invoice_date = $this->cleanString($erpSalesOrder->header->invoice_date);
	            					$invoice->invoice_type = $this->cleanString($erpSalesOrder->header->invoice_type);
	            					$invoice->invoice_document = $this->cleanString($erpSalesOrder->header->invoice_document);
	            					$invoice->line_count = $this->cleanString($erpSalesOrder->header->line_count);
	            					$invoice->total_amount = $this->cleanString($erpSalesOrder->header->total_amount);
	            					$invoice->discount_amount = $this->cleanString($erpSalesOrder->header->discount_amount);
	            					$invoice->vat_amount = $this->cleanString($erpSalesOrder->header->vat_amount);
	            					$invoice->grand_total = $this->cleanString($erpSalesOrder->header->total_amount) + 
	            						$this->cleanString($erpSalesOrder->header->vat_amount) - 
	            						$this->cleanString($erpSalesOrder->header->discount_amount);
	            					$invoice->timestamp = date('Y-m-d H:i:s');
	            					$invoice->created_at = date('Y-m-d H:i:s');
	            					$invoice->updated_at = date('Y-m-d H:i:s');
	            					$invoice->status = INVOICE_PENDING;
	            					$invoice->save();
	            				}else{
	            					if($invoice->status == INVOICE_PENDING){
		            					$invoice->sales_order_no = $this->cleanString($erpSalesOrder->header->sales_order_no);
		            					$invoice->invoice_date = $this->cleanString($erpSalesOrder->header->invoice_date);
		            					$invoice->invoice_type = $this->cleanString($erpSalesOrder->header->invoice_type);
		            					$invoice->invoice_document = $this->cleanString($erpSalesOrder->header->invoice_document);
		            					$invoice->line_count = $this->cleanString($erpSalesOrder->header->line_count);
		            					$invoice->total_amount = $this->cleanString($erpSalesOrder->header->total_amount);
		            					$invoice->discount_amount = $this->cleanString($erpSalesOrder->header->discount_amount);
		            					$invoice->vat_amount = $this->cleanString($erpSalesOrder->header->vat_amount);
		            					$invoice->grand_total = $this->cleanString($erpSalesOrder->header->total_amount) + 
		            						$this->cleanString($erpSalesOrder->header->vat_amount) - 
		            						$this->cleanString($erpSalesOrder->header->discount_amount);
		            					$invoice->timestamp = date('Y-m-d H:i:s');
		            					$invoice->updated_at = date('Y-m-d H:i:s');
		            					$invoice->status = INVOICE_PENDING;
		            					$invoice->save();
		            				}
	            				}

	            				$detailCount = 0;

	            				if(count($erpSalesOrder->details) > 0){
	            					foreach($erpSalesOrder->details as $key => $detail){
	            						$detail->item_code = $this->cleanString($detail->item_code);
	            						$detail->delivery_qty = $this->cleanString($detail->delivery_qty);
	            						$detail->invoice_line = $this->cleanString($detail->invoice_line);
	            						$detail->order_unit = $this->cleanString($detail->order_unit);
	            						$detail->unit_price = $this->cleanString($detail->unit_price);
	            						$detail->line_amount = $this->cleanString($detail->line_amount);
	            						$detail->line_discount = $this->cleanString($detail->line_discount);

	            						$product = Product::where('code', $detail->item_code)->first();

	            						if($product){
		            						$invDetails = InvoiceDetails::where('invoice_id', $invoice->id)
		            							->where('product_id',$product->id)
		            							->where('qty',$detail->delivery_qty)
		            							->where('unit',$detail->order_unit)
		            							->first();

		            						if(!$invDetails){
		            							$invDetails = new InvoiceDetails;
		            							$invDetails->invoice_id = $invoice->id;
		            							$invDetails->product_id = $product->id;
		            							$invDetails->qty = $detail->delivery_qty;
		            							$invDetails->unit = $detail->order_unit;
		            							$invDetails->unit_price = $detail->unit_price;
		            							$invDetails->discount_amount = $detail->line_discount;
		            							$invDetails->net_amount = $detail->line_amount;
		            							$invDetails->invoice_line = $detail->invoice_line;
		            							$invDetails->created_at = date('Y-m-d H:i:s');
		            							$invDetails->updated_at = date('Y-m-d H:i:s');
		            							$invDetails->status = INVOICE_PENDING;
		            							$invDetails->save();
		            							$detailCount++;
		            						}else{

		            							if($invDetails->status == INVOICE_PENDING){
			            							$invDetails->invoice_id = $invoice->id;
			            							$invDetails->product_id = $product->id;
			            							$invDetails->qty = $detail->delivery_qty;
			            							$invDetails->unit = $detail->order_unit;
			            							$invDetails->unit_price = $detail->unit_price;
			            							$invDetails->discount_amount = $detail->line_discount;
			            							$invDetails->net_amount = $detail->line_amount;
			            							$invDetails->invoice_line = $detail->invoice_line;
			            							$invDetails->updated_at = date('Y-m-d H:i:s');
			            							$invDetails->status = INVOICE_PENDING;
			            							$invDetails->save();
			            						}
		            							$detailCount++;
		            						}
		            					}
	            					}

	            					if($detailCount == count($erpSalesOrder->details)){
										$quotation->status = INVOICED;
										$quotation->save();
		            				}
	            				}
            				}
            			}
            		}
            	}*/

            	$currentTimestamp = date('Y-m-d H:i:s');
            	$previousTimestamp = SyncTime::where('name','invoice')->first();

            	$erpSalesOrders = $this->erpLogic->getInvoices($previousTimestamp->last_sync);

            	if(count($erpSalesOrders) > 0){
            		foreach($erpSalesOrders as $key => $erpSalesOrder){
            			$reference = $this->cleanString($erpSalesOrder->header->reference);
            			$quotation = Quotation::where('reference_no',$reference)->first();

            			$invoice = Invoice::where('invoice_no', $this->cleanString($erpSalesOrder->header->invoice_no))->first();

            			$customer = Customer::where('code',$this->cleanString(
            							$erpSalesOrder->header->bpr
            						))->first();

            			$date = new \DateTime($this->cleanString($erpSalesOrder->header->invoice_date), new \DateTimeZone('UTC'));
						$date->setTimezone(new \DateTimeZone('Asia/Colombo'));
						$invoice_date = $date->format('Y-m-d H:i:s');

            			if(!$invoice){
        					$invoice = new Invoice;
        					$invoice->sales_order_no = $this->cleanString($erpSalesOrder->header->sales_order_no);
        					$invoice->invoice_no = $this->cleanString($erpSalesOrder->header->invoice_type).'-'.str_replace(',', '', $this->cleanString($erpSalesOrder->header->invoice_document));
        					$invoice->invoice_date = $invoice_date;
        					$invoice->invoice_type = $this->cleanString($erpSalesOrder->header->invoice_type);
        					$invoice->invoice_document = $this->cleanString($erpSalesOrder->header->invoice_document);
        					$invoice->line_count = $this->cleanString($erpSalesOrder->header->line_count);
        					$invoice->total_amount = $this->cleanString($erpSalesOrder->header->total_amount);
        					$invoice->discount_amount = $this->cleanString($erpSalesOrder->header->discount_amount);
        					$invoice->vat_amount = $this->cleanString($erpSalesOrder->header->vat_amount);
        					$invoice->grand_total = $this->cleanString($erpSalesOrder->header->total_amount) + 
        						$this->cleanString($erpSalesOrder->header->vat_amount) - 
        						$this->cleanString($erpSalesOrder->header->discount_amount);
        					$invoice->timestamp = date('Y-m-d H:i:s');
        					$invoice->created_at = date('Y-m-d H:i:s');
        					$invoice->updated_at = date('Y-m-d H:i:s');

        					if($customer){
        						$invoice->customer_id = $customer->id;
        						$invoice->customer_name = trim($customer->first_name.' '.$customer->last_name);
        					}

        					if($quotation){
        						$invoice->quotation_no = $quotation->reference_no;
        					}

        					$invoice->status = INVOICE_PENDING;
        					$invoice->save();
        				}/*else{
        					if($invoice->status == INVOICE_PENDING){
            					$invoice->sales_order_no = $this->cleanString($erpSalesOrder->header->sales_order_no);
            					$invoice->invoice_date = $this->cleanString($erpSalesOrder->header->invoice_date);
            					$invoice->invoice_type = $this->cleanString($erpSalesOrder->header->invoice_type);
            					$invoice->invoice_document = $this->cleanString($erpSalesOrder->header->invoice_document);
            					$invoice->line_count = $this->cleanString($erpSalesOrder->header->line_count);
            					$invoice->total_amount = $this->cleanString($erpSalesOrder->header->total_amount);
            					$invoice->discount_amount = $this->cleanString($erpSalesOrder->header->discount_amount);
            					$invoice->vat_amount = $this->cleanString($erpSalesOrder->header->vat_amount);
            					$invoice->grand_total = $this->cleanString($erpSalesOrder->header->total_amount) + 
            						$this->cleanString($erpSalesOrder->header->vat_amount) - 
            						$this->cleanString($erpSalesOrder->header->discount_amount);
            					$invoice->timestamp = date('Y-m-d H:i:s');
            					$invoice->updated_at = date('Y-m-d H:i:s');

            					if($customer){
	        						$invoice->customer_id = $customer->id;
	        						$invoice->customer_name = trim($customer->first_name.' '.$customer->last_name);
	        					}

            					if($quotation){
	        						$invoice->quotation_no = $quotation->reference_no;
	        					}

            					$invoice->status = INVOICE_PENDING;
            					$invoice->save();
            				}
        				}*/

        				$detailCount = 0;

        				if(count($erpSalesOrder->details) > 0){
        					foreach($erpSalesOrder->details as $key => $detail){
        						$detail->item_code = $this->cleanString($detail->item_code);
        						$detail->delivery_qty = $this->cleanString($detail->delivery_qty);
        						$detail->invoice_line = $this->cleanString($detail->invoice_line);
        						$detail->order_unit = $this->cleanString($detail->order_unit);
        						$detail->unit_price = $this->cleanString($detail->unit_price);
        						$detail->line_amount = $this->cleanString($detail->line_amount);
        						$detail->line_discount = $this->cleanString($detail->line_discount);

        						$product = Product::where('code', $detail->item_code)->first();

        						if($product){
            						$invDetails = InvoiceDetails::where('invoice_id', $invoice->id)
            							->where('product_id',$product->id)
            							->where('qty',$detail->delivery_qty)
            							->where('unit',$detail->order_unit)
            							->first();

            						$detailCount++;

            						if(!$invDetails){
            							$invDetails = new InvoiceDetails;
            							$invDetails->invoice_id = $invoice->id;
            							$invDetails->product_id = $product->id;
            							$invDetails->qty = $detail->delivery_qty;
            							$invDetails->unit = $detail->order_unit;
            							$invDetails->unit_price = $detail->unit_price;
            							$invDetails->discount_amount = $detail->line_discount;
            							$invDetails->net_amount = $detail->line_amount;
            							$invDetails->invoice_line = $detail->invoice_line;
            							$invDetails->created_at = date('Y-m-d H:i:s');
            							$invDetails->updated_at = date('Y-m-d H:i:s');
            							$invDetails->status = INVOICE_PENDING;
            							$invDetails->save();
            						}/*else{

            							if($invDetails->status == INVOICE_PENDING){
	            							$invDetails->invoice_id = $invoice->id;
	            							$invDetails->product_id = $product->id;
	            							$invDetails->qty = $detail->delivery_qty;
	            							$invDetails->unit = $detail->order_unit;
	            							$invDetails->unit_price = $detail->unit_price;
	            							$invDetails->discount_amount = $detail->line_discount;
	            							$invDetails->net_amount = $detail->line_amount;
	            							$invDetails->invoice_line = $detail->invoice_line;
	            							$invDetails->updated_at = date('Y-m-d H:i:s');
	            							$invDetails->status = INVOICE_PENDING;
	            							$invDetails->save();
	            						}
            							$detailCount++;
            						}*/
            					}
        					}

        					if($detailCount == count($erpSalesOrder->details)){
        						if($quotation){
									$quotation->status = INVOICED;
									$quotation->save();
								}
            				}
        				}
            		}

            		$previousTimestamp->last_sync = $currentTimestamp;
					$previousTimestamp->save();
            	}
            }else{
	            SyncStatus::create([
	                'data' => '',
	                'employee_id' => $user->employee_id,
	                'sync_type_id' => $type,
	                'status' => 0
	            ]);
	            return redirect('admin/erp/list')->with([
	                'error' => true,
	                'error.message' => 'Empty Data !.',
	                'error.title' => 'Warnning!'
	            ]);
	        }
        	// DB::commit();
	        if ($status) {
	            SyncStatus::create([
	                'data' => '',
	                'employee_id' => $user->employee_id,
	                'sync_type_id' => $type,
	                'status' => 1
	            ]);

	            if(!$request->is_terminal){
		            return redirect('admin/erp/list')->with([
		                'success' => true,
		                'success.message' => 'Downloaded successfully!',
		                'success.title' => 'Success...!'
		            ]);
		        }

		        return 'done';
	        }
        
        }
        catch(\Exception $e){
        	return $e;
            return $e->getMessage();
        }
    }

    public function validateDate($date, $format = 'Y-m-d'){
	    $d = \DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function exportView(Request $request)
	{

		$quotations = Quotation::with(['customer'])
							->where('status',CLOSED);

		if($request->start && $request->start != ''){
			$quotations->where('quotation_date', '>=', $request->start.' 00:00:00');
		}

		if($request->end && $request->end != ''){
			$quotations->where('quotation_date', '<=', $request->end.' 00:00:00');
		}

		if($request->quotation_no && $request->quotation_no != ''){
			$quotations->where('reference_no', 'like', '%'.$request->quotation_no.'%');
		}

		if($request->customer && $request->customer != ''){
			$quotations->whereIn('customer_id', function($q) use ($request){
				return $q->select('id')
					->from('customer')
					->where('code', 'like', '%'. $request->customer.'%')
					->orWhere('first_name', 'like', '%'. $request->customer.'%')
					->orWhere('last_name', 'like', '%'. $request->customer.'%');
			});
		}

		$quotations = $quotations->get();

		return view('ERPDataManage::export')->with(['old' => $request, 'quotations' => $quotations]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function export(Request $request){

		//if($request->start){

			try{
				$d = $this->erpLogic->downloadHeader($request);
				$e = $this->erpLogic->downloadHeaderHistory($request);
				$f = $this->erpLogic->downloadLine($request);
				$g = $this->erpLogic->downloadLineHistory($request);

				if($d && $e && $f && $g){
					$this->erpLogic->changeStatusToPOProcessing($request);
					return redirect('admin/erp/export')->with([
						'success' => true,
						'success.title' => 'Success',
						'success.message' => 'Successfully Exported',
					]);
				}
			}catch(\Exception $e){
				return redirect('admin/erp/export')->with([
					'error' => true,
					'error.title' => 'Error Occured',
					'error.message' => $e->getMessage(),
				]);
			}
		/*}else{
			return redirect('admin/erp/export')->with([
				'error' => true,
				'error.title' => 'Error occured',
				'error.message' => 'Could not find start date',
			]);
		}*/
	}

	public function exportCsv(Request $request){

		if($request->type){
			try{
				switch($request->type){
					case 1: 
						return $this->erpLogic->downloadHeader($request,true);
						break;
					case 2: 
						return $this->erpLogic->downloadHeaderHistory($request,true);
						break;
					case 3: 
						return $this->erpLogic->downloadLine($request,true);
						break;
					case 4: 
						return $this->erpLogic->downloadLineHistory($request,true);
						break;
				}
			}catch(\Exception $e){
				return $e->getMessage();
			}
		}else{
			return 'Error';
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function getAllProduct($pageNum)
	{
		$perPage = 2000;
		// $proudctPages = $this->erpLogic->getProductPageCount($perPage);
		return $this->erpLogic->getAllProduct($pageNum,$perPage);	
	}


	public function getAllCustomer($pageNum)
	{
		$perPage = 2000;
		return $this->erpLogic->getProjectCustomers($pageNum,$perPage);	
	}

}
