<?php namespace App\Modules\ERPDataManage\Models;

/**
*
* Model
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SyncType extends Model {

	/**
     * table row delete
     */
    use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sync_type';

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function sync_status(){
		return $this->hasMany('App\Modules\ERPDataManage\Models\SyncStatus', 'sync_type_id', 'id')->whereNull('deleted_at');
	}

}
