<?php

namespace App\Providers;

use App\Classes\Contracts\BaseStockHandler;
use App\Classes\Contracts\Handler;
use App\Classes\StockHandler;
use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
   // protected $defer = true;

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Classes\Contracts\BaseStockHandler', 'App\Classes\StockHandler');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            Handler::class
        ];
    }
}