<?php namespace App\Http\Controllers;

use Core\UserManage\Models\User;
use Sentinel;
use Response;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

use App\Classes\Functions;
use App\Models\AppUser;

use Core\EmployeeManage\Models\Employee;
use Core\EmployeeManage\Models\EmployeeType;
use App\Modules\AdminProductManage\BusinessLogics\ProductLogic;
use App\Modules\AdminOrderManage\BusinessLogics\OrderLogic;
use App\Modules\AdminCustomerManage\BusinessLogics\CustomerLogic;
use App\Modules\AdminDealerManage\Models\Helper;
use App\Modules\AdminOrderManage\Models\Order;

class ApiHelperController extends Controller {


	protected $productLogic;
	protected $orderLogic;
	protected $customerLogic;

	function __construct(ProductLogic $productLogic,OrderLogic $orderLogic,CustomerLogic $customerLogic)
	{
		$this->productLogic = $productLogic;
		$this->orderLogic = $orderLogic;
		$this->customerLogic = $customerLogic;
	}
		
	public function checkUserAuthorize(Request $request)
	{
		if (!isset($_REQUEST['username']) || !isset($_REQUEST['password'])) {
            return 'invalid request request parameters is in valid ';
        }

        $userName = $request->get('username');
        $passWord = $request->get('password');
        $response = array();
        $response["timestamp"] = Functions::unixTimePhpToJava(time());

        $result = AppUser::selectRaw('*,id as userId')
        			->where('username', $userName)
		            ->where('password', md5($passWord))
		            ->where('refernece_type', 'helper')
		            ->first();


        if (count($result) == 1) {
        	$response["result"] = 1;
        	$response["user"] = $result;
            $result->last_login =  date("Y/m/d H:i:s");
            $result->save();
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
	}

	public function getOrders(Request $request){
		if (!isset($_REQUEST['token'])) {
            return 'invalid request request parameters is in valid ';
        }

		$token = $request->get('token');

        if ($token != NULL && $this->isToken($token)) {
			$response              = array();
			$response["timestamp"] = Functions::unixTimePhpToJava(time());
			$response["result"]    = 1;
			$user                  = $this->getTokenUser($token);					

			$helper                = Helper::find($user->reference_no);
			$result 			   = $this->orderLogic->getOrdersHelper($helper->city_id); 						

			$response["orders"]     = $result;
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }			
	}


	public function acceptOrder(Request $request){
		if (!isset($_REQUEST['token'])) {
            return 'invalid request request parameters is in valid ';
        }

		$token = $request->get('token');
		$order_id = $request->get('order_id');

        if ($token != NULL && $this->isToken($token)) {
			$response              = array();
			$response["timestamp"] = Functions::unixTimePhpToJava(time());
			$response["result"]    = 1;
			$user                  = $this->getTokenUser($token);					

			$order                   = Order::find($order_id);
			$helper                  = Helper::find($user->reference_no);
			$order->dealer_id        = $helper->dealer_id;					
			$order->dealer_helper_id = $helper->id;					
			$order->dealer_helper_id = $helper->id;					
			$order->status           = APPROVED;					
			$order->save();				

			$response["order"]       = $order;
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }			
	}









	public function isToken($token)
	{	
		return AppUser::where('token',$token)->count()>0;
	}

	public function getTokenUser($token)
	{	
		return AppUser::where('token',$token)->first();
	}


}
