<?php namespace App\Http\Controllers;

use Core\UserManage\Models\User;
use Sentinel;
use Response;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

use App\Classes\Functions;
use App\Models\AppUser;

use App\Modules\AdminOrderManage\BusinessLogics\OrderLogic;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use LaravelFCM\Message\Topics;
use FCM;
use App\Modules\AdminOrderManage\Models\Order;

use App\Modules\PaymentManage\BusinessLogics\PaymentLogic;
use App\Modules\AdminDealerManage\Models\Dealer;
use App\Modules\AdminDealerManage\Models\Helper;
use Illuminate\Support\Facades\Log;

class ApiOrelPayController extends Controller {


	protected $orderLogic;
	protected $paymentLogic;

	function __construct(OrderLogic $orderLogic,PaymentLogic $paymentLogic)
	{
		$this->orderLogic = $orderLogic;
		$this->paymentLogic =  $paymentLogic;
	}
		

	public function paymentStatus(Request $request){
		//FIRE FCM
		
		
		try {
			Log::info(json_encode($request->all()));

			//FZND THE ORDER 
			//DB::beginTransaction();

			// $order         = $this->orderLogic->getOrderByNo($request->order_no);	
			// $initiate_user = AppUser::where('refernece_type',$order->initiate_user_type)->where('reference_no',$order->order_initiate_by)->first();
			
			// //TRASACTION ABOUT WHAT HAPPEND
			// $this->paymentLogic->orelPayTransaction($order,"TRANSFAKE0000001","SUCCESS","OH YEAH THIS IS DONE");
			
			// //IF PAYMENT DONE 
			// $total_order_amount = $this->orderLogic->getOrderTotal($order->id);
			// $this->paymentLogic->addNewPayment($total_order_amount->TOTAL,$order);



			// $optionBuilder       = new OptionsBuilder();
	  //       $notificationBuilder = new PayloadNotificationBuilder("");
	  //       $notificationBuilder->setBody("Your payment has been done via OrelPay")
	  //                           ->setTitle('BuyOrange')
	  //                           ->setSound('default');


	  //       $dataBuilder                    = new PayloadDataBuilder();
	  //       $dataBuilder->addData(['a_data' => 'payment_done']);
	  //       $option                         = $optionBuilder->build();
	  //       $notification                   = $notificationBuilder->build();
	  //       $data                           = $dataBuilder->build();
	  //       $downstreamResponse             = FCM::sendTo($initiate_user->fcm_token, $option, $notification, $data);
	  //       $downstreamResponse->numberSuccess();
	  //       $downstreamResponse->numberFailure();
	  //       $downstreamResponse->numberModification();

	        // $order->status = ORDER_PAYMENT_DONE;
	        // $order->save();

	        //DB::commit();
		} catch (Exception $e) {
			DB::rollback();
			return $e;
		}

				
	}


}
