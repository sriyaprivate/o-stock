<?php
namespace App\Classes;

/**
 *
 * Overpayment Tranacstions
 *
 * @author Tharindu Lakshan<tahrinudmac@gmail.com>
 * @version 1.0.0
 * @copyright Copyright (c) 2015, Yasith Samarawickrama
 *
 */


use App\Classes\Contracts\BaseStockHandler;
use App\Models\Stock;
use App\Models\StockTransaction;
use App\Models\StockTransactionType;
use App\Models\StockType;

use App\Modules\AdminProductManage\Models\Product;
use App\Models\Warehouse;
use DB;
use Exception;
use Illuminate\Support\Facades\Log;

class StockHandler implements BaseStockHandler
{


    /**
     * invoke new transaction to the stock (add to stocks)
     * @param $product_id
     * @param $qty
     * @param $warehouse_id
     * @param $batch_id
     * @param $transaction_type_id
     * @param $mrp_id
     * @return App\Models\StockTransaction
     * @throws Exception
     */
    public function stockIn($product_id, $qty, $warehouse_id, $batch_id, $transaction_type_id, $mrp_id = 0)
    {
        try {
            $this->validate($product_id, $qty, $warehouse_id, $batch_id, $transaction_type_id, BaseStockHandler::STOCK_IN);
        } catch (\Exception $e) {
            throw new Exception($e);
        }
        $product = $this->getStock($warehouse_id, $product_id, $batch_id);

        if (sizeof($product) == 0) {
            $transaction = DB::transaction(function () use ($product_id, $qty, $warehouse_id, $batch_id, $transaction_type_id, $mrp_id) {

                try {
                    $stock = Stock::create([
                        "product_id" => $product_id,
                        "qty" => $qty,
                        "batch_id" => $batch_id,
                        "warehouse_id" => $warehouse_id,
                        "mrp_id" => $mrp_id,
                        "status" => 1
                    ]);
                    $transaction = $this->transactionIn($qty, $stock, $batch_id, $transaction_type_id);

                } catch (\Exception $e) {
                    throw new Exception($e);
                }
                return $transaction;
            });
            return $transaction;
        } else {
            throw new Exception("Stock Already Exist..");
        }
    }

    /**
     * invoke stock out transation (reduce stocks)
     * @param $product_id
     * @param $qty
     * @param $batch_id
     * @param $warehouse_id
     * @param $reference_id
     * @param $transaction_type_id
     * @param $type
     * @return App\Models\StockTransaction
     * @throws Exception
     */
    public function stockOut($product_id, $qty, $batch_id = 0, $warehouse_id, $reference_id, $transaction_type_id, $type)
    {
        try {
            $this->validate($product_id, $qty, $warehouse_id, $batch_id, $transaction_type_id, $type);
        } catch (\Exception $e) {
            throw new Exception($e);
        }
        $stocks = $this->getStock($warehouse_id, $product_id, $batch_id);

        if (sizeof($stocks) > 0) {
            $transaction = DB::transaction(function () use ($product_id, $qty, $warehouse_id, $reference_id, $transaction_type_id, $stocks, $type) {
                try {
                    if ($type == BaseStockHandler::FIRST_IN_FIRST_OUT) {
                        return $this->first_out($stocks, $qty, 0, $reference_id, $transaction_type_id);
                    } else {
                        return $this->batch_out($stocks, $qty, $reference_id, $transaction_type_id);
                    }

                } catch (\Exception $e) {
                    throw new Exception($e);
                }
            });
            return $transaction;

        } else {
            throw new Exception("Stock Not Exist..");
        }


    }


    /**
     * in transtion to the transations
     * @param  qty
     * @param  stock
     * @param  reference_id
     * @param  type_id
     * @return App\Models\StockTransaction
     */
    private function transactionIn($qty, $stock, $reference_id, $type_id)
    {
        $transaction = StockTransaction::create([
            "qty" => $qty,
            "stock_id" => $stock->id,
            "reference_id" => $reference_id,
            "transaction_type_id" => $type_id
        ]);

        if ($transaction) {
            return $transaction;
        } else {
            throw new Exception("Transaction Error");
        }

    }

    /**
     * out transtion to the transations
     * @param  qty
     * @param  stock
     * @param  reference_id
     * @param  type_id
     * @return App\Models\StockTransaction
     */
    private function transactionOut($qty, $stock, $reference_id, $type_id)
    {
        $transaction = StockTransaction::create([
            "qty" => -$qty,
            "stock_id" => $stock->id,
            "reference_id" => $reference_id,
            "transaction_type_id" => $type_id
        ]);

        if ($transaction) {
            return $transaction;
        } else {
            throw new Exception("Transaction Error");
        }

    }

    private function getStock($warehouse_id, $product_id, $batch_id)
    {
        $stock = Stock::where('warehouse_id', $warehouse_id)->where('product_id', $product_id);
        if ($batch_id > 0) {
            $stock = $stock->where('batch_id', $batch_id);
        } else {
            $stock = $stock->where('qty', '>', 0);
        }
        return $stock = $stock->orderBy('batch_id', 'ASC')->get();
    }

    private function validate($product_id, $qty, $warehouse_id, $batch_id, $transaction_type_id, $type)
    {
        $product = Product::find($product_id);
        if (!$product) {
            throw new Exception("Invalid Product");
        }
        if (!is_integer($qty)) {
            throw new Exception("Invalid Qty");
        }
        $warehouse = Warehouse::find($warehouse_id);
        if (!$warehouse) {
            throw new Exception("Invalid Warehouse");
        }
        $stock_transaction = StockTransactionType::find($transaction_type_id);
        if (!$stock_transaction) {
            throw new Exception("Invalid Transaction Type");
        }
        if ($stock_transaction->reference_tbl && $batch_id > 0) {
            $batch = DB::select(DB::raw('SELECT * FROM ' . $stock_transaction->reference_tbl . ' where id = ' . $batch_id));
            if (!$batch) {
                throw new Exception("Invalid Transaction Type");
            }
        }
        /** for stock out */
        if ($batch_id == 0) {
            $stock_qty = Stock::where('product_id', $product_id)->groupBy('product_id')->sum('qty');
            if ($stock_qty < $qty) {
                throw new Exception("Not Enough Stock");
            }
        } elseif ($type == BaseStockHandler::BATCH_OUT) {
            $stock_qty = Stock::where('product_id', $product_id)->where('batch_id', $batch_id)->groupBy('product_id')->sum('qty');
            if ($stock_qty < $qty) {
                throw new Exception("Not Enough Stock");
            }
        }

    }

    private function first_out($stocks, $qty, $index = 0, $reference_id, $transaction_type_id)
    {
        if ($qty == 0 || (sizeof($stocks) - 1) < $index) {
            return;
        }
        if ($stocks[$index]->qty >= $qty) {
            $updateThis = DB::table('stock')->where('id', $stocks[$index]->id)->decrement('qty', $qty);
            if ($updateThis) {
                $this->transactionOut($qty, $stocks[$index], $reference_id, $transaction_type_id);
            }
            return $updateThis;
        } else {
            $updateThis = DB::table('stock')->where('id', $stocks[$index]->id)->decrement('qty', $stocks[$index]->qty);
            if ($updateThis) {
                $this->transactionOut($stocks[$index]->qty, $stocks[$index], $reference_id, $transaction_type_id);
            }
            $qty -= $stocks[$index]->qty;
            $this->first_out($stocks, $qty, ++$index, $reference_id, $transaction_type_id);
        }

    }

    private function batch_out($stocks, $qty, $reference_id, $transaction_type_id)
    {
        $updateThis = DB::table('stock')->where('id', $stocks[0]->id)->decrement('qty', $qty);
        if ($updateThis) {
            try {
                $transaction = $this->transactionOut($stocks[0]->qty, $stocks[0], $reference_id, $transaction_type_id);
                return $transaction;
            } catch (\Exception $e) {
                throw new Exception($e);
            }
        } else {
            throw new Exception("error");
        }
    }

}
