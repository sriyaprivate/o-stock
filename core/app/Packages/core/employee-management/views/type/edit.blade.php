@extends('layouts.back_master') @section('title','Edit Employee Type')
@section('css')
<style type="text/css">
	.chosen-container{
		font-family: 'FontAwesome', 'Open Sans',sans-serif;
	}
</style>
@stop
@section('content')

<section class="content-header">
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
	  	<li><a href="javascript:;">Employee Management</a></li>
	  	<li class="active">Edit - Employee Type</li>
	</ol>
</section>

<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Edit Employee Type</h3>
		</div>
		<div class="box-body">
			
			<form role="form" class="form-horizontal form-validation" method="post">
	        {!!Form::token()!!}
			
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="row top">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<label class=" control-label required" >Name</label>
							<input type="text" class="form-control @if($errors->has('name')) error @endif" name="name" placeholder="Enter Type Name" required value="{{$type->name}}">
							@if($errors->has('name'))
								<label id="label-error" class="error" for="label">{{$errors->first('name')}}</label>
							@endif
						</div>
					</div>

					<div class="row top">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<label class=" control-label required" >Parent</label>
							{!! Form::select('parent', $parentList,$type->parent,['class'=>'form-control chosen','style'=>'width:100%;font-family:\'FontAwesome\'','data-placeholder'=>'']) !!}
						</div>
					</div>

					<div class="row top">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<button type="submit" class="pull-right btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
						</div>
					</div>
				</div>

			</form>

		</div>
	</div>
</section>
@stop
@section('js')
<script type="text/javascript">
	$(document).ready(function(){
	});
</script>
@stop
