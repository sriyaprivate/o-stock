<?php
namespace Core\EmployeeManage\Http\Requests;

use App\Http\Requests\Request;

class EmployeeRequest extends Request
{
    public function authorize(){
        return true;
    }

    public function rules(){
        $id = $this->id;
        // $repID = $this->uID;
        if($this->is('employee/add')){
            $rules = [
                'email'             => 'email|unique:employee,email,'.$this->email,
                'code'              => 'required|unique:employee,code,'.$this->code
            ];
        }else if($this->is('employee/edit/'.$id)){
            $rules = [
                'email'             => 'email|unique:employee,email,'.$id,
                'code'              => 'required|unique:employee,code,'.$id
            ];
        }else if($this->is('employee/reset/reset-password')){
            $rules = [
                'user_name'         => 'unique:user,username,'.$id,
                'uName'             => 'unique:dealer,username,'.$id,
                'mobile'            => 'required|min:10|max:10',
                'password'          => 'required|min:6|regex:/^((?=\S*?[A-Z])(?=\S*?[a-z])(?=\S*?[0-9]).{6,})\S$/',
                'confirm_password'  => 'required|same:password'
            ];
        }
        return $rules;
    }

}