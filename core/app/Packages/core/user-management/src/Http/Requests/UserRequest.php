<?php
namespace Core\UserManage\Http\Requests;

use App\Http\Requests\Request;
use Input;

class UserRequest extends Request {

	public function authorize(){
		return true;
	}

	public function rules(){  
		$rules = [
			/*'email'					=> 'required|email|unique:users,email',*/
			'employee_id'			=> 'required',
			'sector'				=> 'required',
			'user_type'				=> 'required',
			'first_name'			=> 'required',
			'last_name'				=> 'required',
			'user_name'				=> 'required|min:6|unique:users,username',
			'email'					=> 'required|email|unique:users,email,'.$this->id,
			'password' 				=> 'required|confirmed|min:6',
			'password_confirmation'	=> 'required'
		]; 
		/*if(Input::get('password') != NULL){
			$rules = [
				'first_name' => 'required',
				'last_name' => 'required',
				'email' => 'required|email|unique:users,email',
				'user_name' => 'required|min:5',
				'password' => 'required|confirmed|min:6'
			];
		} else{
			$rules = [
				'first_name' => 'required',
				'last_name' => 'required',
				'email' => 'required|email|unique:users,email,'.Request::segment(3),
				'user_name' => 'required|min:6',	
			];
		}*/
		
		return $rules;
	}

}
