<?php
namespace Core\UserManage\Http\Requests;

use App\Http\Requests\Request;
use Input;

class AppUserRequest extends Request {

	public function authorize(){
		return true;
	}

	public function rules(){  
		$rules = [
			/*'email'					=> 'required|email|unique:users,email',*/
			'employee'				=> 'required',
			'user_name'				=> 'required|min:6|unique:dms_app_user,username',
			'email'					=> 'email|unique:dms_app_user,email',
			'password' 				=> 'required|confirmed|min:6',
			'password_confirmation'	=> 'required'
		]; 
		/*if(Input::get('password') != NULL){
			$rules = [
				'first_name' => 'required',
				'last_name' => 'required',
				'email' => 'required|email|unique:users,email',
				'user_name' => 'required|min:5',
				'password' => 'required|confirmed|min:6'
			];
		} else{
			$rules = [
				'first_name' => 'required',
				'last_name' => 'required',
				'email' => 'required|email|unique:users,email,'.Request::segment(3),
				'user_name' => 'required|min:6',	
			];
		}*/
		
		return $rules;
	}

}
