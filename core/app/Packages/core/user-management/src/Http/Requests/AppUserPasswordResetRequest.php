<?php
namespace Core\UserManage\Http\Requests;

use App\Http\Requests\Request;
use Input;

class AppUserPasswordResetRequest extends Request {

	public function authorize(){
		return true;
	}

	/*public function messages()
	{
	    return [
	        'new_password.confirmed' => 'The password confirmation does not match.'
	    ];
	}*/

	public function rules(){  
		$rules = [
			'password'			=> 'required|min:6|confirmed',
			'password_confirmation'	=> 'required|min:6'
		]; 

		return $rules;
	}
}
