@extends('layouts.back_master') @section('title','Edit User')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<style type="text/css">
	.top{
        margin-top: 10px
    }
</style>
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
	<h1><small></small></h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{url('user/list')}}">Web User Management</a></li>
		<li class="active">Edit Web User</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Edit Web User</h3>
			<div class="box-tools pull-right" style="">
                <a href="{{ URL::previous() }}" class="btn btn-info"><span class="fa fa-caret-left"></span></a>
				<!-- <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
				<button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> -->
			</div>
		</div>
		<div class="box-body">
            <div class="container-fluid">
                <br>
                <form action="{{ url('user/edit', ['id' => request()->id]) }}" method="post">
                    {!! Form::token() !!}
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="row top">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <label class=" control-label required">Employee</label>
                                            
                                    @if($errors->has('employee'))
                                        1111111
                                        <select name="employee" id="employee" class="form-control chosen">
                                            <option value="">-- Select Employee --</option>
                                            @if(sizeof($employees) > 0)
                                                @foreach($employees as $employee)
                                                    @if($curUser['employee_id'] == $employee->id)
                                                        <option value="{{ $employee->id }}" selected>{{ $employee->code." - ".$employee->name }}</option>
                                                    @else
                                                        <option value="{{ $employee->id }}">{{ $employee->code." - ".$employee->name }}</option>
                                                    @endif
                                                @endforeach

                                            @endif
                                        </select>
                                        <label id="employee-error" class="help-block" for="employee">
                                            {{$errors->first('employee')}}
                                        </label>
                                    @else
                                        <select name="employee" id="employee" class="form-control chosen" disabled>
                                            <option value="">-- Select Employee --</option>
                                            @if(sizeof($employees) > 0)
                                                @foreach($employees as $employee)
                                                    @if($curUser['employee_id'] == $employee->id)
                                                        <option value="{{ $employee->id }}" selected>{{ $employee->code." - ".$employee->name }}</option>
                                                    @else
                                                        <option value="{{ $employee->id }}">{{ $employee->code." - ".$employee->name }}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                    @endif
                                </div>
                            </div>

                            <div class="row top">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <label class=" control-label required">User Roles</label>
                                    @if($errors->has('user_type'))
                                        <select name="user_type" id="user_type" class="form-control chosen">
                                            <option value="">-- User Roles --</option>
                                            @if(sizeof($user_types) > 0)
                                                @foreach($user_types as $user_type)
                                                    @if($srole == $user_type->id)
                                                        <option value="{{ $user_type->id }}" selected>{{ $user_type->name }}</option>
                                                    @else
                                                        <option value="{{ $user_type->id }}">{{ $user_type->name }}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                        <label id="user_type-error" class="help-block" for="user_type">
                                            {{$errors->first('user_type')}}
                                        </label>
                                    @else
                                        <select name="user_type" id="user_type" class="form-control chosen">
                                            <option value="">-- User Roles --</option>
                                            @if(sizeof($user_types) > 0)
                                                @foreach($user_types as $user_type)
                                                    @if($srole == $user_type->id)
                                                        <option value="{{ $user_type->id }}" selected>{{ $user_type->name }}</option>
                                                    @else
                                                        <option value="{{ $user_type->id }}">{{ $user_type->name }}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                    @endif
                                </div>
                                <input type="hidden" id="user_log" name="user_log" value="1">
                            </div>

                            @if($curUser['sector_id'] != 0)
                                <div class="row top">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <label class=" control-label required">Sector</label>
                                        @if($errors->has('sectors'))
                                            <select name="sector" id="sector" class="form-control chosen">
                                                <option value="0">-- Sector --</option>
                                                @if(sizeof($sectors) > 0)
                                                    @foreach($sectors as $key=>$sector)
                                                        @if($curUser['sector_id'] == $key)
                                                            <option value="{{ $key }}" selected>{{ $sector }}</option>
                                                        @else
                                                            <option value="{{ $key }}">{{ $sector }}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </select>
                                            <label id="sector-error" class="help-block" for="sector">
                                                {{$errors->first('sector')}}
                                            </label>
                                        @else
                                            <select name="sector" id="sector" class="form-control chosen">
                                                <option value="0">-- Sector --</option>
                                                @if(sizeof($sectors) > 0)
                                                    @foreach($sectors as $key=>$sector)
                                                        @if($curUser['sector_id'] == $key)
                                                            <option value="{{ $key }}" selected>{{ $sector }}</option>
                                                        @else
                                                            <option value="{{ $key }}">{{ $sector }}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </select>
                                        @endif
                                    </div>
                                </div>
                            @endif

                            <div class="row top">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <label class="control-label required">User Name</label>
                                    <input type="text" class="form-control " name="user_name" placeholder="User Name" value="{{$curUser['username']}}">
                                    @if($errors->has('user_name'))
                                        <label id="label-error" class="help-block" for="label">{{$errors->first('user_name')}}</label>
                                    @endif                                  
                                </div>

                            </div>

                            <div class="row top" style="margin-bottom: 10px;">
                                <div class="col-lg-12">
                                    <div class="pull-right">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
		</div>
	</div>	
</section>	

@stop
@section('js')


<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function() {
  $(".chosen").chosen();
});
	
</script>
@stop
