<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * Menu Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Tharindu Lakshan <tharindumac@gmail.com>
 * @copyright  Copyright (c) 2015, tharindu lakshan
 * @version    v1.0.0
 */
class StockTransactionType extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'stock_transactions_types';
	
	 /**
     * The attributes that are not assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

	

}
