<?php
namespace App\Models;

use Cartalyst\Sentinel\Permissions\PermissibleInterface;
use Cartalyst\Sentinel\Permissions\PermissibleTrait;
use Cartalyst\Sentinel\Persistences\PersistableInterface;
use Cartalyst\Sentinel\Roles\RoleableInterface;
use Cartalyst\Sentinel\Roles\RoleInterface;
use Cartalyst\Sentinel\Users\UserInterface;

use Illuminate\Database\Eloquent\Model;
use Baum\Node;

class AppUser extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'app_user';


	// guard attributes from mass-assignment
	protected $guarded = array('id');

	public function employee(){
		return $this->belongsTo('Core\EmployeeManage\Models\Employee','employee_id','id');
	}

}
