<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * Menu Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Tharindu Lakshan <tharindumac@gmail.com>
 * @copyright  Copyright (c) 2015, tharindu lakshan
 * @version    v1.0.0
 */
class Stock extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'stock';
	
	 /**
     * The attributes that are not assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];


    public function product()
    {
    	return $this->hasMany('App\Modules\AdminProductManage\Models\Product','id','product_id');
    }

    public function mrp()
    {
        return $this->belongsTo('Core\PriceBook\Models\Mrp','mrp_id','id');
    }

    public function warehouse()
    {
        return $this->belongsTo('App\Modules\Warehouse\Models\Warehouse','warehouse_id','id');
    }
}
