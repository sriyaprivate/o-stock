<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Nbt extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */

    protected $table = 'nbt';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];



}
