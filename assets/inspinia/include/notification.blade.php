<li class="dropdown">
    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
        <i class="fa fa-bell"></i>  <span class="label label-primary">8</span>
    </a>
    <ul class="dropdown-menu dropdown-alerts">
        <li>
            <div class="dropdown-messages-box">
                <a href="profile.html" class="pull-left">
                    <img alt="image" class="img-circle" src="img/a7.jpg">
                </a>
                <div class="media-body">
                    <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>
                    <small class="text-muted">3 days ago</small>
                </div>
            </div>
        </li>
        <li class="divider"></li>
        <li>
            <div class="dropdown-messages-box">
                <a href="profile.html" class="pull-left">
                    <img alt="image" class="img-circle" src="img/a4.jpg">
                </a>
                <div class="media-body ">
                    <strong>Chris Johnatan Overtunk</strong> started following <strong>Monica Smith</strong>. <br>
                    <small class="text-muted">Yesterday</small>
                </div>
            </div>
        </li>
        <li class="divider"></li>
        <li>
            <div class="dropdown-messages-box">
                <a href="profile.html" class="pull-left">
                    <img alt="image" class="img-circle" src="img/a3.jpg">
                </a>
                <div class="media-body ">
                    <strong>Monica Smith</strong> love <strong>Kim Smith</strong>. <br>
                    <small class="text-muted">2 days ago</small>
                </div>
            </div>
        </li>
        <li class="divider"></li>
        <li>
            <div class="text-center link-block">
                <a href="mailbox.html">
                    <i class="fa fa-envelope"></i> <strong>View All Notifications</strong>
                </a>
            </div>
        </li>
    </ul>
</li>